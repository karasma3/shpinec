<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StandingsExport implements FromView {
    /**
     * StandingsExport constructor.
     */
    public function __construct($standingsTable) {
        $this->standingsTable = $standingsTable;
    }

    public function view(): View {
        return view('partials.standingsExport', [
            'standingsTable' => $this->standingsTable
        ]);
    }
}
