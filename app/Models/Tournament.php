<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Class Tournament
 *
 * @package App\Models
 */
class Tournament extends Model {
    const GROUP_NAME = 'A';
    const PHASE_CREATED = 'created';
    const PHASE_GROUP_STAGE = 'group_stage';
    const PHASE_ELIMINATION_STAGE = 'elimination_stage';
    const PHASE_CLOSED = 'closed';
    const PLAYERS_IN_BRACKET = [8, 16, 32, 64];
	const MAX_PLAYER_COUNT = 64;

    public function semester() {
        return $this->belongsTo(Semester::class);
    }

    public function allGroupsAndBrackets() {
        return $this->hasMany(Group::class)->orderBy('round');
    }

    public function groups() {
        return $this->hasMany(Group::class)->where('round', '=', 0);
    }

    public function groupsOrderedByWinRatio() {
        return $this->hasMany(Group::class)->where('round', '=', 0)->get()->sortByDesc(function (Group $group) {
            return $group->getFirstWinRatio();
        });
    }

    public function groupsInRandomOrder() {
        return $this->hasMany(Group::class)->where('round', '=', 0)->inRandomOrder();
    }

    public function brackets() {
        return $this->hasMany(Group::class)->where([['round', '>', 0], ['loser_bracket', '=', false]])->orderBy('round');
    }

    public function firstRoundBracket() {
        return $this->hasMany(Group::class)->where([['round', '=', 1], ['loser_bracket', '=', false]]);
    }

    public function loserBrackets() {
        return $this->hasMany(Group::class)->where([['round', '>', 0], ['loser_bracket', '=', true]])->orderBy('round');
    }

    public function firstRoundLoserBracket() {
        return $this->hasMany(Group::class)->where([['round', '=', 1], ['loser_bracket', '=', true]]);
    }

    public function teamsInGroupStage() {
        return $this->belongsToMany(Team::class)->where('elimination_stage', '=', false);
    }

    public function teamsAfterGroupStageOrdered() {
        $groups = $this->groupsOrderedByWinRatio();
        $teams = [];
        foreach ($groups as $group) {
            if ($group->getWinners()) {
                array_push($teams, $group->getWinners()[0]);
            }
        }
        foreach ($groups as $group) {
            if (count($group->getWinners()) >= 2) {
                array_push($teams, $group->getWinners()[1]);
            }
        }
        return $teams;
    }

    public function losersAfterGroupStageOrdered() {
        $groups = $this->groupsOrderedByWinRatio();
        $teams = [];
        foreach ($groups as $group) {
            $losers = $group->getLosers();
            if ($losers) {
                array_push($teams, $losers[0]);
            }
        }
        foreach ($groups as $group) {
            $losers = $group->getLosers();
            if (count($losers) >= 2) {
                array_push($teams, $losers[1]);
            }
        }
        return $teams;
    }

    public function teamsInEliminationStage() {
        return $this->belongsToMany(Team::class)->where('elimination_stage', '=', true);
    }

    public function matchesInBracket($brackets) {
        $matches = collect();
        foreach ($brackets as $bracket) {
            foreach ($bracket->matches as $match) {
                $matches->push($match);
            }
        }
        $matches = $matches->sortBy(function ($match) {
            return $match->match_number;
        });
        return $matches;
    }

    public function teams() {
        return $this->belongsToMany(Team::class);
    }

    public function teamsOrderedByRanking() {
        return $this->belongsToMany(Team::class)->get()->sortByDesc(function (Team $team) {
            return $team->getWinRatio();
        });
    }

    public function isFinalTournament(){
        return $this->final_tournament;
    }

    public function isCreated() {
        if ('created' == $this->phase) return true;
        return false;
    }

    public function isGroupStage() {
        if ('group_stage' == $this->phase) return true;
        return false;
    }

    public function isEliminationStage() {
        if ('elimination_stage' == $this->phase) return true;
        return false;
    }

    public function isClosed() {
        if ('closed' == $this->phase) return true;
        return false;
    }

    public function getState() {
        switch ($this->phase) {
            case self::PHASE_GROUP_STAGE:
                return trans('localization.tournament_phase_group_stage');
            case self::PHASE_ELIMINATION_STAGE:
                return trans('localization.tournament_phase_elimination_stage');
            case self::PHASE_CLOSED:
                return trans('localization.tournament_phase_closed');
            default:
                return trans('localization.tournament_phase_registration');
        }
    }

    public function setNextState() {
        switch ($this->phase) {
            case self::PHASE_GROUP_STAGE:
                $this->phase = self::PHASE_ELIMINATION_STAGE;
                $this->save();
                return;
            case self::PHASE_ELIMINATION_STAGE:
                $this->phase = self::PHASE_CLOSED;
                $this->save();
                return;
            case self::PHASE_CLOSED:
                return;
            default:
                $this->phase = self::PHASE_GROUP_STAGE;
                $this->save();
                return;
        }
    }

    public function getTournamentName(){
        if ($this->final_tournament) {
            return trans('localization.final_tournament');
        } else {
            return $this->tournament_number . '. ' . trans('localization.tournament_lowercase');
        }
    }

    public function addTeam(int $team_id) {
        if (!$this->teams()->find($team_id)) {
            $this->teams()->attach($team_id);
            session()->flash('message', trans('localization.msg_tournament_register'));
        } else {
            session()->flash('fail', trans('localization.fail_tournament_register'));
        }
    }

    public function removeTeam(int $team_id) {
        if (!$this->teams()->find($team_id)) {
            session()->flash('fail', trans('localization.fail_tournament_remove_registration'));
        } else {
            $this->teams()->detach($team_id);
            session()->flash('message', trans('localization.msg_tournament_remove_registration'));
        }
    }

    public function createGroups(int $size) {
        // delete if previously generated
        $group_ids = DB::table('groups')->select('id')->where('tournament_id', '=', $this->id)->get()->toArray();
        foreach ($group_ids as $group_id) {
            DB::table('matches')->where('group_id', '=', $group_id->id)->delete();
        }
        $this->groups()->delete();
        // create new groups
        $groups = [[]];
        for ($i = 0; $i < $size - 1; $i++) {
            array_push($groups, $tmp_teams = []);
        }
        $group_name = self::GROUP_NAME;
        foreach ($groups as $group) {
            $new_group = Group::create([
                'tournament_id' => $this->id,
                'group_name' => $group_name,
                'round' => 0
            ]);
            $group_name++;
        }
    }

    public function generateGroups() {
        // delete if previously generated
        $group_ids = DB::table('groups')->select('id')->where('tournament_id', '=', $this->id)->get()->toArray();
        foreach ($group_ids as $group_id) {
            DB::table('matches')->where('group_id', '=', $group_id->id)->delete();
        }
        $this->groups()->delete();
        // create groups by 4 teams
        $sum_teams = count($this->teams);
        $sum_groups = ceil($sum_teams / 4);
        // seeding into groups
        $team_ids = $this->teamsOrderedByRanking();
        $groups = [[]];
        for ($i = 0; $i < $sum_groups - 1; $i++) {
            array_push($groups, $tmp_teams = []);
        }
        $i = 0;
        foreach ($team_ids as $team) {
            array_push($groups[$i], $team);
            $i++;
            if ($i == $sum_groups) {
                $i = 0;
            }
        }
        // create groups
        $group_name = self::GROUP_NAME;
        foreach ($groups as $group) {
            $new_group = Group::create([
                'tournament_id' => $this->id,
                'group_name' => $group_name,
                'round' => 0
            ]);
            $group_name++;
            foreach ($group as $team) {
                $new_group->teams()->save($team);
            }
            //create matches
            $new_group->generateMatches();
        }
        $this->phase = self::PHASE_GROUP_STAGE;
        $this->save();
        session()->flash('message', trans('localization.msg_tournament_groups_create'));
    }

    // manualne nastavovanie turnaja
    /*
    public function generateMatchesInGroups() {
        foreach ($this->groups as $group) {
            // NOT VALIDATED groups - full/correct
            $group->generateMatches();
        }
        session()->flash('message', trans('localization.msg_tournament_matches_create'));
    }
    */

    public function createBracket($ordered_teams, $is_loser_bracket) {
        $bracket_size = 0;
        $cnt_players = count($ordered_teams);
        foreach (self::PLAYERS_IN_BRACKET as $players_ceil) {
            if ($players_ceil <= $cnt_players) {
                $bracket_size = $players_ceil;
            }
        }
        $bracket = Group::create([
            'tournament_id' => $this->id,
            'group_name' => 'Kolo: 1',
            'round' => 1,
            'loser_bracket' => $is_loser_bracket
        ]);
        if ($bracket_size != $cnt_players) {
            $ordered_teams = array_pad($ordered_teams, $bracket_size * 2, null);
        }
        $number_of_rounds = log(count($ordered_teams) / 2, 2);
        for ($i = 0; $i < $number_of_rounds; $i++) {
            $out = array();
            $splice = pow(2, $i);
            while (count($ordered_teams) > 0) {
                $out = array_merge($out, array_splice($ordered_teams, 0, $splice));
                $out = array_merge($out, array_splice($ordered_teams, -$splice));
            }
            $ordered_teams = $out;
        }
        $match_number = 1;
        for ($i = 0; $i < count($ordered_teams); $i = $i + 2) {
            if ($ordered_teams[$i + 1] != null) {
                $score = Score::create([
                ]);
                $match = Match::create([
                    'score_id' => $score->id,
                    'group_id' => $bracket->id,
                    'team_id_first' => $ordered_teams[$i]->id,
                    'team_id_second' => $ordered_teams[$i + 1]->id,
                    'match_number' => $match_number
                ]);
            } else {
                $score = Score::create([
                    'score_first' => null,
                    'score_second' => null
                ]);
                $match = Match::create([
                    'score_id' => $score->id,
                    'group_id' => $bracket->id,
                    'team_id_first' => $ordered_teams[$i]->id,
                    'team_id_second' => null,
                    'match_number' => $match_number,
                    'played' => true
                ]);
            }
            DB::table('team_tournament')->where([['team_id', $ordered_teams[$i]->id], ['tournament_id', $this->id]])->update(['elimination_stage' => true]);
            if (!$bracket->findTeam($ordered_teams[$i]->id)) {
                $bracket->teams()->save($ordered_teams[$i]);
            }
            if ($ordered_teams[$i + 1] != null) {
                DB::table('team_tournament')->where([['team_id', $ordered_teams[$i + 1]->id], ['tournament_id', $this->id]])->update(['elimination_stage' => true]);
                if (!$bracket->findTeam($ordered_teams[$i + 1]->id)) {
                    $bracket->teams()->save($ordered_teams[$i + 1]);
                }
            }
            $match_number++;
        }
        $this->phase = 'elimination_stage';
        $this->save();
        session()->flash('message', trans('localization.msg_tournament_bracket_create'));
    }

    public function nextRound() {
        $last_round = Group::find(Group::select('id')->where([['tournament_id', $this->id], ['loser_bracket', false]])->orderBy('round', 'desc')->first())->first();
        if ($last_round->is_finale) {
            session()->flash('fail',  trans('localization.fail_tournament_final_round_exists'));
            return;
        }
        foreach ($last_round->matches as $match) {
            if (!$match->played) {
                session()->flash('fail',  trans('localization.fail_tournament_all_matches'));
                return;
            }
        }
        $next_round = Group::create([
            'tournament_id' => $this->id,
            'round' => $last_round->round + 1,
            'group_name' => 'Kolo: ' . ($last_round->round + 1)
        ]);
        $next_round->generateMatchesInBracket($last_round);
        session()->flash('message', trans('localization.msg_tournament_next_round'));
    }

    public function nextLoserBracket(){
        $last_round = Group::find(Group::select('id')->where([['tournament_id', $this->id], ['loser_bracket', true]])->orderBy('round', 'desc')->first())->first();
        if ($last_round->is_finale) {
            session()->flash('fail',  trans('localization.fail_tournament_final_round_exists'));
            return;
        }
        foreach ($last_round->matches as $match) {
            if (!$match->played) {
                session()->flash('fail',  trans('localization.fail_tournament_all_matches'));
                return;
            }
        }
        $next_round = Group::create([
            'tournament_id' => $this->id,
            'round' => $last_round->round + 1,
            'group_name' => 'Kolo: ' . ($last_round->round + 1),
            'loser_bracket' => true
        ]);
        $next_round->generateMatchesInBracket($last_round);
        session()->flash('message', trans('localization.msg_tournament_next_round'));
    }

    public function getEmails() {
        $emails = '';
        foreach ($this->teams as $team) {
            $emails = $emails . $team->getEmails();
        }
        return $emails;
    }

    public function getEmailsElimination() {
        $emails = '';
        foreach ($this->teamsInEliminationStage as $team) {
            $emails = $emails . $team->getEmails();
        }
        return $emails;
    }

    public function getStandingsTable() {
        $standingsTable = [];
        foreach ($this->allGroupsAndBrackets as $groupOrBracket) {
            $groupOrBracket->calculateScore();
            foreach ($this->teams as $team) {
                $teamStandings = $groupOrBracket->getPointsAndScoreOfTeam($team->id);
                if (is_null($teamStandings) || is_null($teamStandings->score_won)) {
                    continue;
                }
                if (array_key_exists($team->id, $standingsTable)) {
                    $standingsTable[$team->id]->score_won += $teamStandings->score_won;
                    $standingsTable[$team->id]->score_lost += $teamStandings->score_lost;
                    $standingsTable[$team->id]->matches_all += $groupOrBracket->countMatchesAllForTeam($team->id);
                    $standingsTable[$team->id]->matches_wins += $groupOrBracket->countMatchesWinsForTeam($team->id);
                    $standingsTable[$team->id]->matches_loses += $groupOrBracket->countMatchesLosesForTeam($team->id);
                    if ($groupOrBracket->round == 0) {
                        $standingsTable[$team->id]->group_points += $teamStandings->points;
                        $standingsTable[$team->id]->group_score_won += $teamStandings->score_won;
                        $standingsTable[$team->id]->group_score_lost += $teamStandings->score_lost;
//                        if ($groupOrBracket->isTeamWinner($team->id)) {
//                            $standingsTable[$team->id]->points += 2; // +2 points for advancing into elimination stage
//                        } else {
//                            $standingsTable[$team->id]->points += 1; // +1 point for competing in tournament
//                        }
                    }else{
                        $standingsTable[$team->id]->points = $groupOrBracket->getPointsInBracket($team->id, $this->brackets->count()); // + points for standings
                    }
                } else {
                    $standingsTable[$team->id] = $teamStandings;
                    if ($groupOrBracket->round == 0) {
                        $standingsTable[$team->id]->{"group_points"} = $teamStandings->points;
                        $standingsTable[$team->id]->{"group_score_won"} = $teamStandings->score_won;
                        $standingsTable[$team->id]->{"group_score_lost"} = $teamStandings->score_lost;
//                        if ($groupOrBracket->isTeamWinner($team->id)) {
//                            $standingsTable[$team->id]->points = 2; // +2 points for advancing into elimination stage
//                        } else {
//                            $standingsTable[$team->id]->points = 1; // +1 point for competing in tournament
//                        }
                        $standingsTable[$team->id]->points = 0;
                    } else {
                        $standingsTable[$team->id]->points = $groupOrBracket->getPointsInBracket($team->id, $this->brackets->count()); // + points for standings
                        $standingsTable[$team->id]->{"group_points"} = 0;
                        $standingsTable[$team->id]->{"group_score_won"} = 0;
                        $standingsTable[$team->id]->{"group_score_lost"} = 0;
                    }
                    $standingsTable[$team->id]->{"matches_all"} = $groupOrBracket->countMatchesAllForTeam($team->id);
                    $standingsTable[$team->id]->{"matches_wins"} = $groupOrBracket->countMatchesWinsForTeam($team->id);
                    $standingsTable[$team->id]->{"matches_loses"} = $groupOrBracket->countMatchesLosesForTeam($team->id);
                }
            }
        }
        usort($standingsTable, function ($a, $b) {
            if ($a->points == $b->points) {
                if($a->score_won == $b->score_won){
                    return $a->score_lost > $b->score_lost ? 1 : -1;
                }
                return $a->score_won < $b->score_won ? 1 : -1;
            };
            return $a->points < $b->points ? 1 : -1;
        });
        return $standingsTable;
    }

    public function hasAllMatchesBeenPlayed(){
        foreach ($this->allGroupsAndBrackets as $group){
            if (!$group->hasAllMatchesBeenPlayed()) {
                return false;
            }
        }
        return true;
    }

    public function hasFinalMatch(){
        foreach ($this->brackets as $bracket){
            if($bracket->hasFinalMatch()){
                return true;
            }
        }
        return false;
    }

    public function getFormattedTournamentDate(){
        $date = date_create($this->tournament_date);
        return date_format($date, "d.m.Y G:i");
    }
}
