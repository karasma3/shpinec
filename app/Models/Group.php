<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Class Group
 *
 * @package App\Models
 */
class Group extends Model {
    const POINTS_FIRST = 10;
    const POINTS_SECOND = 9;
    const POINTS_THIRD = 8;
    const POINTS_FOURTH = 7;
    const POINTS_FIFTH_TO_EIGHTH = 6;
    const POINTS_NINTH_TO_SIXTEENTH = 5;
    const POINTS_OTHERS = 5;
    const GET_QUARTER_FINAL_ROUND = 2;
    const GET_EIGHTH_FINAL_ROUND = 3;
    const POINTS_LOSER_BRACKET_FIRST = 4;
    const POINTS_LOSER_BRACKET_SECOND = 3;
    const POINTS_LOSER_BRACKET_THIRD_TO_FOURTH = 2;
    const POINTS_LOSER_BRACKET_OTHERS = 1;

    public function isLoserBracket(){
        return $this->loser_bracket;
    }

    public function tournament() {
        return $this->belongsTo(Tournament::class);
    }

    public function teams() {
        return $this->belongsToMany(Team::class);
    }

    public function matches() {
        return $this->hasMany(Match::class);
    }

    public function matchesInOrder() {
        return $this->hasMany(Match::class)->orderBy('match_number');
    }

    public function addTeam(int $team_id) {
        if (!$this->teams()->find($team_id)) {
            $this->teams()->attach($team_id);
            session()->flash('message', trans('localization.msg_group_register'));

        } else {
            session()->flash('fail', trans('localization.fail_group_register'));
        }
    }

    public function calculateScore() {
        foreach ($this->teams as $team) {
            $points = 0;
            $score_won = 0;
            $score_lost = 0;
            foreach ($team->matches as $match) {
                if ($match->group_id != $this->id ||
                    is_null($match->teamSecondName())) {
                    continue;
                }
                if ($match->team_id_first == $team->id) {
                    $score_won += $match->scoreFirst();
                    $score_lost += $match->scoreSecond();
                    if ($match->wonFirst()) {
                        $points += 1;
                    }
                }
                if ($match->team_id_second == $team->id) {
                    $score_lost += $match->scoreFirst();
                    $score_won += $match->scoreSecond();
                    if ($match->wonSecond()) {
                        $points += 1;
                    }
                }
            }
            DB::table('group_team')->where([['group_id', $this->id], ['team_id', $team->id]])->update(['points' => $points, 'score_won' => $score_won, 'score_lost' => $score_lost]);
        }
    }

    public function getMatchesAllForTeam(int $teamId){
        return $this->matches()->where('group_id', $this->id)->where(function($query) use ($teamId){$query->where('team_id_first', $teamId)->orWhere('team_id_second', $teamId);});
    }

    public function countMatchesAllForTeam(int $teamId){
        return $this->getMatchesAllForTeam($teamId)->count();
    }

    public function countMatchesWinsForTeam(int $teamId){
        return $this->getMatchesAllForTeam($teamId)->get()->filter(function ($match) use($teamId) {
            if ($match->team_id_first == $teamId) {
                return $match->wonFirst();
            }
            if ($match->team_id_second == $teamId) {
                return $match->wonSecond();
            }
        })->values()->count();
    }

    public function countMatchesLosesForTeam(int $teamId){
        return $this->getMatchesAllForTeam($teamId)->get()->filter(function ($match) use($teamId){
            if ($match->team_id_first == $teamId) {
                return !$match->wonFirst();
            }
            if ($match->team_id_second == $teamId) {
                return !$match->wonSecond();
            }
        })->values()->count();
    }

    public function showOrdering() {
        foreach ($this->teams as $team) {
            if ($team->buildScore($this->id) != "0:0") {
                return true;
            }
        }
        return false;
    }

    public function calculateOrder() {
        $teams = DB::table('group_team')->select('team_id', 'points', 'score_won', 'score_lost')->where('group_id', $this->id)->orderByRaw('points DESC, score_won DESC, score_lost')->get();
        $ordering = 0;
        $skip = 0;
        $tmp_team = null;
        foreach ($teams as $team) {
            if ($tmp_team) {
                if ($tmp_team->points == $team->points and $tmp_team->score_won == $team->score_won and $tmp_team->score_lost == $team->score_lost) {
                    $skip++;
                    DB::table('group_team')->where([['group_id', $this->id], ['team_id', $team->team_id],])->update(['ordering' => $ordering]);
                    $tmp_team = $team;
                    continue;
                } else {
                    $skip = 0;
                }
            }
            $ordering += $skip + 1;
            DB::table('group_team')->where([['group_id', $this->id], ['team_id', $team->team_id]])->update(['ordering' => $ordering]);
            $tmp_team = $team;
        }
    }

    public function generateMatches() {
        foreach ($this->teams as $team_one) {
            foreach ($this->teams as $team_two) {
                $first_id = min($team_one->id, $team_two->id);
                $second_id = max($team_one->id, $team_two->id);
                if ($first_id != $second_id) {
                    if (!Match::where([['group_id', '=', $this->id], ['team_id_first', '=', $first_id], ['team_id_second', '=', $second_id]])->exists()) {
                        $score = Score::create([
                        ]);
                        $match = Match::create([
                            'team_id_first' => $first_id,
                            'team_id_second' => $second_id,
                            'group_id' => $this->id,
                            'score_id' => $score->id
                        ]);
                    }
                }
            }
        }
        // return $groupController->generateMatches($this);
    }

    public function generateMatchesInBracket(Group $last_round) {
        $match_number = $last_round->matches->max('match_number') + 1;
        $tmp_match = null;
        $i = 0;
        $tmp_is_finale = null;
        if ($last_round->matches->count() == 2) {
            $this->is_finale = true;
            $this->save();
            $tmp_is_finale = true;
        }
        foreach ($last_round->matchesInOrder as $match) {
            if ($tmp_match) {
                if ($i % 2 != 0) {
                    $score = Score::create([
                    ]);
                    Match::create([
                        'score_id' => $score->id,
                        'group_id' => $this->id,
                        'team_id_first' => $tmp_match->getWinnerId(),
                        'team_id_second' => $match->getWinnerId(),
                        'match_number' => $match_number,
                        'is_finale' => $tmp_is_finale
                    ]);
                    $match_number++;
                    $this->teams()->save(Team::find($tmp_match->getWinnerId()));
                    $this->teams()->save(Team::find($match->getWinnerId()));
                    if ($this->is_finale) {
                        $score = Score::create([
                        ]);
                        Match::create([
                            'score_id' => $score->id,
                            'group_id' => $this->id,
                            'team_id_first' => $tmp_match->getLoserId(),
                            'team_id_second' => $match->getLoserId(),
                            'match_number' => $match_number,
                            'is_finale' => false
                        ]);
                        $match_number++;
                        $this->teams()->save(Team::find($tmp_match->getLoserId()));
                        $this->teams()->save(Team::find($match->getLoserId()));
                    }
                }
            }
            $tmp_match = $match;
            $i++;
        }
    }

    public function findTeam($team_id) {
        return DB::table('group_team')->select('team_id')->where([['group_id', $this->id], ['team_id', $team_id]])->first();
    }

    public function findMatchBySingleTeam($team1) {
        return Match::where([['group_id', '=', $this->id], ['team_id_first', '=', $team1]])->orWhere([['group_id', '=', $this->id], ['team_id_second', '=', $team1]])->get();
    }

    public function findMatch($team1, $team2) {
        return Match::where([['group_id', '=', $this->id], ['team_id_first', '=', $team1], ['team_id_second', '=', $team2]])->orWhere([['group_id', '=', $this->id], ['team_id_first', '=', $team2], ['team_id_second', '=', $team1]])->get();
    }

    public function isGroup() {
        if ($this->round == 0) {
            return true;
        }
        return false;
    }

    public function getWinners() {
        $winners = [];
        $teams = DB::table('group_team')->select('team_id', 'ordering')->where('group_id', $this->id)->orderBy('ordering')->get();
        foreach ($teams as $team) {
            if ($team->ordering > 0 and $team->ordering <= 2) {
                array_push($winners, Team::find($team->team_id));
            }
        }
        return $winners;
    }

    public function getLosers() {
        $losers = [];
        $teams = DB::table('group_team')->select('team_id', 'ordering')->where('group_id', $this->id)->orderBy('ordering')->get();
        foreach ($teams as $team) {
            if ($team->ordering > 2 and $team->ordering <= 4) {
                array_push($losers, Team::find($team->team_id));
            }
        }
        return $losers;
    }

    public function isTeamWinner($team_id) {
        if (in_array(Team::find($team_id), $this->getWinners())) {
            return true;
        }
        return false;
    }

    public function getPointsInBracket($team_id, $cnt_rounds) {
        if ($this->round == 0) {
            return 0;
        }
        if ($this->is_finale) {
            foreach ($this->matches as $match) {
                if ($match->is_finale) {
                    if ($match->getWinnerId() == $team_id) {
                        if($this->isLoserBracket()){
                            return self::POINTS_LOSER_BRACKET_FIRST;
                        } else{
                            return self::POINTS_FIRST;
                        }
                    }
                    if ($match->getLoserId() == $team_id) {
                        if($this->isLoserBracket()){
                            return self::POINTS_LOSER_BRACKET_SECOND;
                        } else {
                            return self::POINTS_SECOND;
                        }
                    }
                } else {
                    if($this->isLoserBracket()){
                        return self::POINTS_LOSER_BRACKET_THIRD_TO_FOURTH;
                    } else {
                        if ($match->getWinnerId() == $team_id) {
                            return self::POINTS_THIRD;
                        }
                        if ($match->getLoserId() == $team_id) {
                            return self::POINTS_FOURTH;
                        }
                    }
                }
            }
        } else {
            if($this->isLoserBracket()){
                return self::POINTS_LOSER_BRACKET_OTHERS;
            }
            $match = $this->findMatchBySingleTeam($team_id)->first();
            if ($cnt_rounds - self::GET_QUARTER_FINAL_ROUND == $this->round) {
                if ($match->getLoserId() == $team_id) {
                    return self::POINTS_FIFTH_TO_EIGHTH;
                }
            }
            if ($cnt_rounds - self::GET_EIGHTH_FINAL_ROUND == $this->round) {
                if ($match->getLoserId() == $team_id) {
                    return self::POINTS_NINTH_TO_SIXTEENTH;
                }
            }
        }
        return self::POINTS_OTHERS;
    }

    public function getFirstWinRatio() {
        $teams = $this->getWinners();
        if ($teams) {
            $team = Team::find($teams[0]->id);
            if ($team) {
                return $team->getWinRatio();
            }
        }
        return 0.5;
    }

    public function getPointsAndScoreOfTeam(int $teamId) {
        return DB::table('group_team')->select('team_id', 'points', 'score_won', 'score_lost')->where([['group_id', $this->id], ['team_id', $teamId]])->first();
    }

    public function hasAllMatchesBeenPlayed(){
        foreach ($this->matches as $match) {
            if (!$match->played) {
                return false;
            }
        }
        return true;
    }

    public function hasFinalMatch(){
        foreach ($this->matches as $match){
            if($match->is_finale){
                return true;
            }
        }
        return false;
    }
}
