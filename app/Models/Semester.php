<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model {
    protected $fillable = array('semester_name');

    public function tournaments() {
        return $this->hasMany(Tournament::class);
    }

    public function teamsInTournament(int $tournamentId) {
        return $this->hasMany(Tournament::class)->where("id", "=", $tournamentId)->first()->teams;
    }

    public function setActive(){
        if ($this->active) {
            $this->active = false;
        } else {
            $this->active = true;
        }
        $this->save();
    }

    public function hasFinalTournament(){
        if($this->tournaments()->where('final_tournament', true)->exists()){
            return true;
        }
        return false;
    }

    public function getFinalTournament(){
        return $this->tournaments()->where('final_tournament', true)->first();
    }

    public function getEmails() {
        $tmp_emails = [];
        foreach ($this->tournaments as $tournament) {
            foreach ($this->teamsInTournament($tournament->id) as $team) {
                array_push($tmp_emails, $team->getEmails());
            }
        }
        $tmp_emails = array_unique($tmp_emails);
        $emails = '';
        foreach ($tmp_emails as $tmp_email) {
            $emails = $emails . $tmp_email;
        }
        return $emails;
    }

    public function getStandingsTable() {
        $standingsTable = [];
        foreach ($this->tournaments as $tournament) {
            if (!$tournament->isClosed() || $tournament->isFinalTournament()) {
                continue;
            }
            $tournamentStandingsTable = $tournament->getStandingsTable();
            foreach ($tournamentStandingsTable as $item) {
                if (array_key_exists($item->team_id, $standingsTable)) {
                    $standingsTable[$item->team_id]->count += 1;
                    if ($item->points < $standingsTable[$item->team_id]->min_points) {
                        $standingsTable[$item->team_id]->min_group_score_lost = $item->group_score_lost;
                        $standingsTable[$item->team_id]->min_group_score_won = $item->group_score_won;
                        $standingsTable[$item->team_id]->min_group_points = $item->group_points;
                        $standingsTable[$item->team_id]->min_score_lost = $item->score_lost;
                        $standingsTable[$item->team_id]->min_score_won = $item->score_won;
                        $standingsTable[$item->team_id]->min_points = $item->points;
                    }
                    $standingsTable[$item->team_id]->points += $item->points;
                    $standingsTable[$item->team_id]->score_won += $item->score_won;
                    $standingsTable[$item->team_id]->score_lost += $item->score_lost;
                    $standingsTable[$item->team_id]->group_points += $item->group_points;
                    $standingsTable[$item->team_id]->group_score_won += $item->group_score_won;
                    $standingsTable[$item->team_id]->group_score_lost += $item->group_score_lost;
                } else {
                    $standingsTable[$item->team_id] = $item;
                    $standingsTable[$item->team_id]->{"min_group_score_lost"} = $item->group_score_lost;
                    $standingsTable[$item->team_id]->{"min_group_score_won"} = $item->group_score_won;
                    $standingsTable[$item->team_id]->{"min_group_points"} = $item->group_points;
                    $standingsTable[$item->team_id]->{"min_score_lost"} = $item->score_lost;
                    $standingsTable[$item->team_id]->{"min_score_won"} = $item->score_won;
                    $standingsTable[$item->team_id]->{"min_points"} = $item->points;
                    $standingsTable[$item->team_id]->{"count"} = 1;
                }
            }
        }
        foreach ($standingsTable as $item) {
            if ($item->count > 3) {
                $item->group_score_lost -= $item->min_group_score_lost;
                $item->group_score_won -= $item->min_group_score_won;
                $item->group_points -= $item->min_group_points;
                $item->score_lost -= $item->min_score_lost;
                $item->score_won -= $item->min_score_won;
                $item->points -= $item->min_points;
            }
        }
        usort($standingsTable, function ($a, $b) {
            if ($a->points == $b->points) {
                if ($a->score_won == $b->score_won) {
                    return $a->score_lost > $b->score_lost ? 1 : -1;
                }
                return $a->score_won < $b->score_won ? 1 : -1;
            };
            return $a->points < $b->points ? 1 : -1;
        });
        return $standingsTable;
    }
}
