<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Player
 *
 * @package App\Models
 */
class Player extends Authenticatable {
    use Notifiable;
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'role'
    ];
    protected $table = 'players';

    public function getFullNameAttribute() {
        return $this->name . ' ' . $this->surname;
    }

    public function singlesTeam(){
        return $this->hasMany(Team::class, 'player_id_first')->orWhere('player_id_second', $this->id)->where('player_id_second', '=', null)->first();
    }

    public function teams() {
        return $this->hasMany(Team::class, 'player_id_first')->orWhere('player_id_second', $this->id);
    }

    public function isAdmin() {
        return $this->role == 'admin' ? true : false;
    }

    public function isPlayer() {
        return ($this->role == 'player' || $this->isOrganizer()) ? true : false;
    }

    public function isOrganizer() {
        return ($this->role == 'organizer' || $this->isAdmin()) ? true : false;
    }

    public function isInactive() {
        return $this->role == 'inactive' ? true : false;
    }

    public function participant($match) {
        foreach ($this->teams as $team) {
            if ($team->id == $match->team_id_first or $team->id == $match->team_id_second) {
                return true;
            }
        }
        return false;
    }
}
