<?php

namespace App\Models;
/**
 * Class Match
 *
 * @package App\Models
 */
class Match extends Model {
    public function score() {
        return $this->belongsTo(Score::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }

    public function tournament(){
        return $this->group->tournament;
    }

    public function teamFirst() {
        return $this->belongsTo(Team::class, 'team_id_first');
    }

    public function teamSecond() {
        return $this->belongsTo(Team::class, 'team_id_second');
    }

    public function buildName() {
        return $this->teamFirstName() . ' vs ' . $this->teamSecondName();
    }

    public function teamFirstName() {
        if (is_null($this->teamFirst)) {
            return null;
        }
        return $this->teamFirst->team_name;
    }

    public function teamSecondName() {
        if (is_null($this->teamSecond)) {
            return null;
        }
        return $this->teamSecond->team_name;
    }

    public function wonFirst() {
        if (is_null($this->scoreFirst())) {
            return true;
        }
        if ($this->scoreFirst() == 0 and $this->scoreSecond() == 0) {
            return false;
        }
        if ($this->scoreFirst() > $this->scoreSecond()) {
            return true;
        }
        return false;
    }

    public function wonSecond() {
        if (is_null($this->scoreSecond())) {
            return false;
        }
        if ($this->scoreFirst() == 0 and $this->scoreSecond() == 0) {
            return false;
        }
        if ($this->scoreFirst() < $this->scoreSecond()) {
            return true;
        }
        return false;
    }

    public function getWinnerId() {
        if ($this->wonFirst()) {
            return $this->team_id_first;
        }
        if ($this->wonSecond()) {
            return $this->team_id_second;
        }
    }

    public function getLoserId() {
        if ($this->wonFirst()) {
            return $this->team_id_second;
        }
        if ($this->wonSecond()) {
            return $this->team_id_first;
        }
    }

    public function scoreFirst() {
        return $this->score->score_first;
    }

    public function scoreSecond() {
        return $this->score->score_second;
    }

    public function buildResult() {
        if ($this->played) {
            return $this->scoreFirst() . ':' . $this->scoreSecond();
        } else {
            return 'TBD';
        }
    }

    public function buildReverseResult() {
        if ($this->played) {
            return $this->scoreSecond() . ':' . $this->scoreFirst();
        } else {
            return 'TBD';
        }
    }

    public function oddMatch() {
        if ($this->match_number % 2 != 0) {
            return true;
        }
        return false;
    }

    public function submitScore(int $score_first, int $score_second) {
        $switch = false;
        if ($this->played) {
            if (($score_first > $score_second && $this->score->score_first < $this->score->score_second) ||
                ($score_first < $score_second && $this->score->score_first > $this->score->score_second)) {
                $switch = true;
            }
        }
        $this->score->score_first = $score_first;
        $this->score->score_second = $score_second;
        $this->score->save();
        $this->teamFirst->updateWinRatio($score_first, $score_second, $this->played, $switch);
        $this->teamSecond->updateWinRatio($score_second, $score_first, $this->played, $switch);
        $this->played = true;
        $this->save();
    }
}
