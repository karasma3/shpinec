<?php

namespace App\Http\Controllers;

use App\Models\Player;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Class PlayerController
 *
 * @package App\Http\Controllers
 */
class PlayerController extends Controller {

    public function show(Player $player) {

        return view('players.show', compact('player'));
    }

    /**
     * Method for editing player - validates name, surname and email after that it updates Player
     *
     * @param Player $player
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editPlayer(Player $player) {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255'
        ]);

        if($player->email != request('email')){
            $this->validate(request(), [
                'email' => 'unique:players'
            ]);
        }

        $player->update(['name' => request('name'), 'surname' => request('surname'), 'email' => request('email')]);
        session()->flash('message', trans('localization.msg_player_info_change'));
        return redirect('/players/' . $player->id);
    }

    /**
     * Method for changing the current password - checks for current password and validates the new password
     *
     * @param Player $player
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(Player $player) {

        $this->validate(request(), [
            'current-password' => 'required',
            'password' => 'required|string|min:5|confirmed'
        ]);
        if (!Hash::check(request('current-password'), Auth::user()->password)) {
            session()->flash('fail', trans('localization.fail_password_wrong'));
        } else {
            $player->update(['password' => Hash::make(request('password'))]);
            session()->flash('message', trans('localization.msg_password_change'));
        }
        return redirect('/players/' . $player->id);
    }

    public function store() {

    }
}
