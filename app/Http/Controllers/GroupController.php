<?php

namespace App\Http\Controllers;

use App\Models\Group;

/**
 * Class GroupController
 *
 * @package App\Http\Controllers
 */
class GroupController extends Controller {

    public function show(Group $group) {

        return view('groups.show', compact('group'));
    }

    public function addTeam(Group $group){
        if (auth()->check()) {
            $this->validate(request(), [
                'team_id' => 'required|numeric'
            ]);
            if (count($group->teams) >= 4) {
                session()->flash('fail', trans('localization.fail_group_upper_limit'));
                return redirect('/groups/' . $group->id);
            }
            $group->addTeam(request('team_id'));
        }
        return redirect('/groups/' . $group->id);
    }
}
