<?php

namespace App\Http\Controllers;

use App\Models\Semester;
use App\Models\Tournament;

class SemesterController extends Controller {
    public function index() {
        $semesters = Semester::all();
        $activeSemester = Semester::where('active', true)->first();
        return view('semesters.index', ['semesters' => $semesters, 'activeSemester' => $activeSemester]);
    }

    public function show(Semester $semester) {
        $standingsTable = $semester->getStandingsTable();
        return view('semesters.show', ['semester' => $semester, 'standingsTable' => $standingsTable]);
    }

    public function export(Semester $semester){
        $standingsTable = $semester->getStandingsTable();
        return \Maatwebsite\Excel\Facades\Excel::download(new \App\Exports\StandingsExport($standingsTable), str_replace('/', '_', $semester->semester_name) . '.csv');
    }

    public function delete(Semester $semester){
        Semester::destroy($semester->id);
        return redirect('/semesters/');
    }

    public function setActive(Semester $semester){
        $semester->setActive();
        return redirect('/semesters/' . $semester->id);
    }

    /**
     * Method for creating Tournament into the Semester
     *
     * @param Semester $semester
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addTournament(Semester $semester) {
        if (auth()->check()) {
            // validacia zaverecneho turnaja
            if($semester->hasFinalTournament()){
                session()->flash('fail', trans('localization.fail_final_tournament_exists'));
                return redirect('/semesters/' . $semester->id);
            }
            if(request('final_tournament')) {
                $final_tournament = true;
            }else {
                $final_tournament = false;
            }
            $tournament = Tournament::create([
                'semester_id' => $semester->id,
                'tournament_number' => $semester->tournaments()->count() + 1,
                'final_tournament' => $final_tournament
            ]);
            $semester->tournaments()->save($tournament);
        }
        return redirect('/semesters/' . $semester->id);
    }

    /**
     * Store method - validates Semester name
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store() {
        $this->validate(request(), [
            'semester_season' => 'required|min:2|max:2|string',
            'semester_year' => 'required|min:9|max:9|string'
        ]);
        $semester_name = request('semester_season') . request('semester_year');
        if(Semester::where('semester_name', $semester_name)->count() == 0) {
            $semester = Semester::create([
                'semester_name' => $semester_name
            ]);
            session()->flash('message', trans('localization.msg_semester_created'));
        } else{
            session()->flash('fail', trans('localization.fail_semester_exists'));
        }
        return redirect('/semesters');
    }
}
