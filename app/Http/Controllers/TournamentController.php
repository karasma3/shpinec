<?php

namespace App\Http\Controllers;

use App\Models\Tournament;
use Illuminate\Support\Facades\Auth;

/**
 * Class TournamentController
 *
 * @package App\Http\Controllers
 */
class TournamentController extends Controller {
    const GROUP_NAME = 'A';
    const GROUP_SIZE = 4;
    const GROUP_STAGE = 'group_stage';

    const TOURNAMENT_LOWER_LIMIT = 8;
    const TOURNAMENT_UPPER_LIMIT = 64;
    const TEAMS_ADVANCING_FROM_GROUP_STAGE_LOWER_LIMIT = 4;

    public function show(Tournament $tournament) {
        $standingsTable = [];
        if($tournament->isClosed()){
            $standingsTable = $tournament->getStandingsTable();
        }
        return view('tournaments.show', ['tournament' => $tournament, 'standingsTable' => $standingsTable]);
    }

    public function export(Tournament $tournament){
        if(!$tournament->isClosed()){
            return back();
        }
        $standingsTable = $tournament->getStandingsTable();
        return \Maatwebsite\Excel\Facades\Excel::download(new \App\Exports\StandingsExport($standingsTable), $tournament->tournament_number . '_turnaj.csv');
    }

    public function saveInfo(Tournament $tournament){
        $this->validate(request(), [
            'from' => 'required|date',
            'locality' => 'required|string'
        ]);
        $tournament->update(['tournament_date' => request('from'), 'tournament_locality' => request('locality')]);
        session()->flash('message', trans('localization.msg_tournament_info_saved'));
        return redirect()->back();
    }

    /**
     * Method for adding Team into the Tournament - validates the signing in Team
     *
     * @param Tournament $tournament
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addTeam(Tournament $tournament) {
        if (auth()->check()) {
            $this->validate(request(), [
                'team_id' => 'required|numeric|integer'
            ]);
            if (count($tournament->teams) >= self::TOURNAMENT_UPPER_LIMIT) {
                session()->flash('fail', trans('localization.fail_tournament_upper_limit', ['count' => self::TOURNAMENT_UPPER_LIMIT]));
            }else{
                $tournament->addTeam(request('team_id'));
            }
        }
        return redirect()->back();
    }

    public function removeTeam(Tournament $tournament){
        if (auth()->check() && (Auth::user()->isOrganizer() || Auth::user()->singlesTeam()->id == request('team_id'))) {
            $this->validate(request(), [
                'team_id' => 'required|numeric|integer'
            ]);
            $tournament->removeTeam(request('team_id'));
        }
        return redirect()->back();
    }

    // manualne nastavovanie turnaja
    /*
    public function setNextState(Tournament $tournament){
        // use with caution
        $tournament->setNextState();
        return redirect('/tournaments/' . $tournament->id . '/edit');
    }


    public function createGroups(Tournament $tournament) {
        $tournament->createGroups(ceil(count($tournament->teams)/4));
        return redirect('tournaments/' . $tournament->id . '/edit');
    }

    public function createMatches(Tournament $tournament) {
        $tournament->generateMatchesInGroups();
        return redirect('tournaments/' . $tournament->id . '/edit');
    }
    */

    /**
     * Creating X Groups depending on number of Teams signed in to Tournament
     *
     * @param Tournament $tournament
     * @var $group_ids - all ids of groups previously generated to be deleted
     * @var $team_ids - stores all the Team id associated with $tournament, in random order
     * @var $groups - multidimensional array, stores X Groups by 4 Teams
     * @var $group - 1 Group with 4 Teams
     * @var $team - Team
     * @var $new_group - newly created Group
     * @var $group_name - generating names for group, starting from 'A'
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateGroups(Tournament $tournament) {
        if($tournament->teams()->count() < self::TOURNAMENT_LOWER_LIMIT){
            session()->flash('fail', trans('localization.fail_tournament_lower_limit', ['count' => self::TOURNAMENT_LOWER_LIMIT]));
        }else{
            $tournament->generateGroups();
        }
        return redirect()->back();
    }

    public function createBracket(Tournament $tournament) {
        $this->calculateScore($tournament);
        if(sizeof($tournament->teamsAfterGroupStageOrdered()) >= self::TEAMS_ADVANCING_FROM_GROUP_STAGE_LOWER_LIMIT) {
            if(!$tournament->hasAllMatchesBeenPlayed()){
                session()->flash('fail', trans('localization.fail_tournament_all_matches'));
            } else{
                $tournament->createBracket($tournament->teamsAfterGroupStageOrdered(), false);
                $tournament->createBracket($tournament->losersAfterGroupStageOrdered(), true);
            }
        }else{
            session()->flash('fail', trans('localization.fail_tournament_bracket_lower_limit', ['count' => self::TEAMS_ADVANCING_FROM_GROUP_STAGE_LOWER_LIMIT]));
        }
        return redirect()->back();
    }

    public function nextRound(Tournament $tournament) {
        $tournament->nextRound();
        return redirect()->back();
    }

    public function nextLoserBracket(Tournament $tournament) {
        $tournament->nextLoserBracket();
        return redirect()->back();
    }

    public function calculateScore(Tournament $tournament) {
        foreach ($tournament->groups as $group) {
            $group->calculateScore();
            if($group->isGroup()) {
                $group->calculateOrder();
            }
        }
        return redirect()->back();
    }

    public function close(Tournament $tournament) {
        if(!$tournament->hasAllMatchesBeenPlayed()){
            session()->flash('fail', trans('localization.fail_tournament_all_matches'));
        }else {
            if(!$tournament->hasFinalMatch()){
                session()->flash('fail', trans('localization.fail_tournament_not_final_round'));
            }else {
                $tournament->phase = 'closed';
                $tournament->save();
                session()->flash('message', trans('localization.msg_tournament_close'));
            }
        }
        return redirect()->back();
    }

    public function showLoserBracket(Tournament $tournament) {
        if($tournament->loserBrackets->isEmpty()){
            return redirect()->back();
        }
        return view('tournaments.loserBracket', ['tournament' => $tournament]);
    }
}
