<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\Team;
use Illuminate\Support\Facades\Auth;

/**
 * Class TeamController
 *
 * @package App\Http\Controllers
 */
class TeamController extends Controller {
    public function __construct() {
        //        $this->middleware('guest', ['only' => 'index', 'show']);
    }

    public function show(Team $team) {
        return view('teams.show', compact('team'));
    }

    public function create() {
        return view('teams.create');
    }

    public function inactivateTeam(Team $team) {
        $team->activate(false);
        return redirect('/teams/' . $team->id);
    }

    /**
     * Store method for DOUBLES teams - validates first email and second email
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store() {
        $this->validate(request(),[
            'email_first' => 'required|string|email|max:255',
            'email_second' => 'required|string|email|max:255'
        ]);
        $player_first = Player::where('email', request('email_first'))->first();
        $player_second = Player::where('email', request('email_second'))->first();
        if ($player_first) {
            if($player_second) {
                // validacia unikatnosti
                if(
                    Team::where([['player_id_first',$player_first->id],['player_id_second',$player_second->id]])->first()!=null ||
                    Team::where([['player_id_first',$player_second->id],['player_id_second',$player_first->id]])->first()!=null
                ){
                    session()->flash('fail', trans('localization.fail_team_exists'));
                    return redirect('/');
                }
                $team = Team::create([
                    'team_name' => $player_first->surname . '/' . $player_second->surname,
                    'player_id_first' => $player_first->id,
                    'player_id_second' => $player_second->id,
                    'singles' => false
                ]);
                session()->flash('message', trans('localization.msg_team_created'));
                return redirect('/teams/' . $team->id);
            } else {
                session()->flash('fail', trans('localization.fail_doubles_player_not_found', ['email' => request('email_second')]));
            }
        } else {
            session()->flash('fail', trans('localization.fail_doubles_player_not_found', ['email' => request('email_first')]));
        }
        return redirect()->back();
    }
}
