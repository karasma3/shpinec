<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfPlayerRemovesThemselfs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $teamId = $request->input('team_id');
        if(Auth::user()->singlesTeam()->id == $teamId or Auth::user()->isOrganizer()){
            return $next($request);
        }
        session()->flash('fail', trans('localization.fail_no_permission'));
        return redirect()->back();
    }
}
