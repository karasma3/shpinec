<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfMatchIsPlayedByTeam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $match = $request->route('match');
        if((Auth::user()->participant($match) and !$match->tournament()->isClosed()) or Auth::user()->isOrganizer()){
            return $next($request);
        }
        session()->flash('fail', trans('localization.fail_no_permission'));
        return redirect()->back();
    }
}
