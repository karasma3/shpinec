<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('team_name');
            $table->boolean('active')->default(true);
            $table->boolean('singles')->default(true);
            $table->integer('player_id_first');
            $table->integer('player_id_second')->nullable();
            $table->unique(['player_id_first', 'player_id_second']);
            $table->integer('won_matches')->default(0);
            $table->integer('played_matches')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('teams');
    }
}
