<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('semester_id');
            $table->integer('tournament_number')->defalut(0);
            $table->enum('phase', ['created', 'group_stage', 'elimination_stage', 'closed'])->default('created');
            $table->boolean('final_tournament')->default(false);
            $table->timestamp('tournament_date')->nullable()->default(null);
            $table->string('tournament_locality')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tournaments');
    }
}
