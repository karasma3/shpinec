# Project SH pinec

## Startup

1.  checkout project from GitLab
2.  install latest version of PHP from https://www.php.net/downloads.php
3.  install latest version of PostgreSQL from https://www.postgresql.org/download/
4.  you need to edit your php.ini file (php.ini file section)
5.  create DB - remember DB name, username and password
6.  use terminal to install composer dependancies - `composer install`
7.  use terminal to generate key for your enviroment - `php artisan key:generate`
8.  you need to edit your .env file in project root directory (.env file section)
9.  use terminal to startup your application - `php artisan serve`
10. restore your DB from `database\backup` directory using PgAdmin 4 tool, or terminal

### php.ini file
uncomment rows:
```
extension_dir= ... ; either for linux or windows
extension=php_fileinfo.dll
extension=gd2
extension=pdo_pgsql
extension=pgsql
```


### .env file
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE= // of your choosing
DB_USERNAME= // of your choosing
DB_PASSWORD= // of your choosing
```