@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="content_headline">
            {!! trans('localization.group')!!}: {{ $group -> group_name }}
        </div>

    @if(Auth::check() and Auth::user()->isAdmin())
        <div class="content_dark">
            <div class="content_inner">
                <div class="boxes_wrap white_links">
                <span class="headline_lw" style="font-weight: bold">
                    {!! trans('localization.admin_section')!!}
                </span>
                <br>
                <br>
{{--                manualne pridavanie hracov do skupin    --}}
{{--                        --}}
{{--                @if($group->tournament->isCreated())--}}
{{--                    <form method="POST" action="/groups/{{$group->id}}/join">--}}
{{--                        @csrf--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="team_id">--}}
{{--                                {!! trans('localization.team')!!}--}}
{{--                            </label>--}}
{{--                            <br>--}}
{{--                            <select type="text" class="form-control"  id="team_id" name="team_id">--}}
{{--                                <option disabled selected>--}}
{{--                                    {!! trans('localization.select_option')!!}--}}
{{--                                </option>--}}
{{--                                @foreach( $group->tournament->teams->sortBy('team_name') as $team)--}}
{{--                                    <option value="{{ $team->id }}">--}}
{{--                                        {{ $team->team_name }}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="button_submit">--}}
{{--                            <span class="button_link_medium button_admin">--}}
{{--                                {!! trans('localization.add_team')!!}--}}
{{--                            </span>--}}
{{--                        </button>--}}
{{--                    </form>--}}
{{--                @endif--}}
                </div>
            </div>
        </div>
    @endif
    <span class="headline_lw">
        {!! trans('localization.quick_navigation')!!}
    </span>
    <br>
    <div class="boxes_wrap white_links">
        <a href="/tournaments/{{ $group->tournament->id }}" class="button_link">
            <span class="button_link_small">
                {!! trans('localization.back_to_tournament')!!}
            </span>
        </a>
    </div>
{{--TODO doubles--}}
    @if(Auth::user()->isAdmin())
{{--remove if admin--}}
        <span class="headline_lw">
        {!! trans('localization.teams')!!}
        </span>
                <br>
                <br>
        @foreach($group->teams as $team)
            <div class="form-group ml-md-5">
                <a href="/teams/{{ $team->id }}">{{ $team->team_name }}</a>
            </div>
        @endforeach
    @endif
    <span class="headline_lw">
        {!! trans('localization.table')!!}
    </span>
    <br>
    <br>
    @include('partials.table')
    <span class="headline_lw">
        {!! trans('localization.matches')!!}
    </span>
    <br>
    <br>
    @foreach($group->matches as $match)
        @if(!$match->played)
            <div class="form-group ml-md-5">
                <a href="/matches/{{ $match->id }}">{{ $match->buildName() }}</a>
            </div>
        @endif
    @endforeach
</div>
    @include('partials.errors')
@endsection