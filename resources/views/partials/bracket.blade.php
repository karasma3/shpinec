<div style="width: 100%; text-align: center">
	<div class="elimination_bracket" style="display: inline-block; overflow-x: auto; margin: 0 auto; text-align: left;">
	</div>
</div>

@section('scripts')
    <script>
        var matchData ={
            teams:[

            ],
            results:[
            ]
        }
        @foreach($brackets as $bracket)
                var score =[]
            @foreach($bracket->matchesInOrder as $match)
                @if(!is_null($match->scoreSecond()))
                    score.push([{{$match->scoreFirst()}}, {{$match->scoreSecond()}}, {{$match->id}}]);
                @else
                    score.push([null, null, ""]);
                @endif
            @endforeach
                matchData.results.push(score);
        @endforeach
        @foreach($tournament->matchesInBracket($firstRoundBracket) as $match)
            @if(!is_null($match->teamSecondName()))
                matchData.teams.push(["{{$match->teamFirstName()}}", "{{$match->teamSecondName()}}"]);
            @else
                matchData.teams.push(["{{$match->teamFirstName()}}", null]);
            @endif
        @endforeach

        function onclick(matchId) {
            if(matchId) {
                location.href = "/matches/" + matchId
            }
        }

        $('.elimination_bracket').bracket({
            init: matchData,
            onMatchClick: onclick
        })
	{{--
        @if( $tournament->teams->count() >32 )
            $('.jQBracket').css('width','1050px');
            $('.jQBracket').css('height','1040px');
        @elseif($tournament->teams->count() >16)
            $('.jQBracket').css('width','850px');
            $('.jQBracket').css('height','520px');
        @else
            $('.jQBracket').css('width','650px');
            $('.jQBracket').css('height','260px');
        @endif
	--}}
	    $('.jQBracket').css('width','');
        $('.round').css('width','150px');
        $('.team').css('width','150px');
        $('.label').css('width','120px');
    </script>
@stop
