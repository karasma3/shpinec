<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-center">
            {!! trans('localization.order')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.player')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.score_in_group')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.points_in_group')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.score_total')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.points_total')!!}
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($standingsTable as $item)
        <tr>
            <td class="text-center">{{$loop->iteration}}</td>
            <td class="text-center">{{\App\Models\Team::find($item->team_id)->team_name}}</td>
            <td class="text-center">{{$item->group_score_won}} : {{$item->group_score_lost}}</td>
            <td class="text-center">{{$item->group_points}} </td>
            <td class="text-center">{{$item->score_won}} : {{$item->score_lost}}</td>
            <td class="text-center">{{$item->points}} </td>
        </tr>
    @endforeach
    </tbody>
</table>