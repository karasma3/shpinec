<table class="table table-bordered group_table">
    <thead>
    <tr>
        <th class="text-center">
            <a href="/groups/{{$group->id}}">
                {{$group->group_name}}
            </a>
        </th>
        @foreach($group->teams->sortBy('team_name') as $team)
            <th style="white-space:normal;" class="text-center">
                {{$team->team_name}}
            </th>
        @endforeach
        <th class="text-center score_col">
            {!! trans('localization.wins')!!}
        </th>
        <th class="text-center score_col">
            {!! trans('localization.score')!!}
        </th>
        <th class="text-center score_col">
            {!! trans('localization.order')!!}
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($group->teams->sortBy('team_name') as $team)
        @if( $group->showOrdering() && $team->showOrder($group->id)<=2 )
            <tr style="background-color:#fff8ba">
        @else
            <tr>
        @endif
            <th style="white-space:normal;" class="text-center">
                {{$team->team_name}}
            </th>
            @foreach($group->teams->sortBy('team_name') as $opponent)
                @if($team->id==$opponent->id)
                    <td class="text-center" bgcolor="#a9a9a9">
                        X
                    </td>
                @else

                    @if($group->findMatch($team->id,$opponent->id)->first())
                        @if($group->findMatch($team->id,$opponent->id)->first()->team_id_first == $team->id)
                            <td class="text-center">
                                <a href="/matches/{{$group->findMatch($team->id,$opponent->id)->first()->id}}">
                                    {{$group->findMatch($team->id,$opponent->id)->first()->buildResult()}}
                                </a>
                            </td>
                        @else
                            <td class="text-center">
                                <a href="/matches/{{$group->findMatch($team->id,$opponent->id)->first()->id}}">
                                    {{$group->findMatch($team->id,$opponent->id)->first()->buildReverseResult()}}
                                </a>
                            </td>
                        @endif
                    @endif
                @endif
            @endforeach
            <td class="text-center score_col">
                {{$team->showPoints($group->id)}}
            </td>
            <td class="text-center score_col">
                {{$team->buildScore($group->id)}}
            </td>
            @if(!$group->showOrdering())
                <td></td>
            @else
                <td class="text-center score_col">
                    {{$team->showOrder($group->id)}}
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>