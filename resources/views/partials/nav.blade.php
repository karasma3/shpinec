<div id="header">
	<div class="header_strip">
		
		<span class="header_button_box">
			<a href="/#">
				{!! trans('localization.nav_home')!!}
			</a>
		</span>
		
		<span class="header_button_box">
			<a href="/semesters">
				{!! trans('localization.nav_tournaments')!!}
			</a>
		</span>
		<!-- TODO STATS
		<span class="header_button_box">
			<a href="/stats">
				{!! trans('localization.nav_stats')!!}
			</a>
		</span>
		-->
		
		
{{--
            <a class="nav-link" href="/">Domov</a>
            <a class="nav-link" href="/semesters">Semestre</a>
            <a class="nav-link" href="/teams">Tímy</a>
--}}        
        </div>
		<div class="header_button_box" style="position: absolute; right: 5px;">
		@php $locale = session()->get('locale'); @endphp
		@switch($locale)
            @case('cs')
				<a href="/lang/en" style="display: inline-block;">
	            	<span><img src="{{asset('img/flag_cz.png')}}" width="30px" height="20px" title="{!! trans('localization.nav_switch_language')!!}"></span>
				</a>
            @break
            @case('en')
            	<a href="/lang/cs">
            	<span><img src="{{asset('img/flag_uk.png')}}" width="30px" height="20px" title="{!! trans('localization.nav_switch_language')!!}"></span>
				</a>
            @break
            @default
            	<a href="/lang/en">
	            	<span><img src="{{asset('img/flag_cz.png')}}" width="30px" height="20px" title="{!! trans('localization.nav_switch_language')!!}"></span>
				</a>
		@endswitch

		</div>
		@if(Auth::check())
			<span class="header_button_box" style="float: right; margin-right: 45px">
				<a href="/logout">
					{!! trans('localization.nav_logout')!!}
				</a>
			</span>
        	@else
			<span class="header_button_box" style="float: right; margin-right: 45px">
				<a href="/login">
					{!! trans('localization.nav_login')!!}
				</a>
			</span>

        	@endif	
			
		@if(Auth::check())
			<span class="header_button_box" style="float: right" >
				<a href="/players/{{Auth::id()}}">
					{{Auth::user()->name}} {{Auth::user()->surname}}
				</a>
			</span>
		@endif
    </div>
</div>
