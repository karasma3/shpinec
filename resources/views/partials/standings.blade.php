<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-center">
            {!! trans('localization.order_short')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.player')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.matches_all')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.matches_wins')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.matches_loses')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.score')!!}
        </th>
        <th class="text-center">
            {!! trans('localization.points')!!}
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($standingsData as $item)

        @if(!isset($hideAdvancingPlayers) && $loop->iteration <= 8)
            <tr style="background-color:#fff8ba">
        @else
            <tr>
        @endif
            <td class="text-center">{{$loop->iteration}}</td>
            <td class="text-center">
{{--                TODO doubles tournament link for teams--}}
{{--                <a href="/teams/{{$item->team_id}}">{{\App\Models\Team::find($item->team_id)->team_name}}</a>--}}
                <a href="/players/{{\App\Models\Team::find($item->team_id)->player_id_first}}">
                    {{\App\Models\Team::find($item->team_id)->team_name}}
                </a>
            </td>
            <td class="text-center">{{$item->matches_all}} </td>
            <td class="text-center">{{$item->matches_wins}} </td>
            <td class="text-center">{{$item->matches_loses}} </td>
            <td class="text-center">{{$item->score_won}}:{{$item->score_lost}}</td>
            <td class="text-center">{{$item->points}} </td>
        </tr>
    @endforeach
    </tbody>
</table>
