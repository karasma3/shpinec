@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.player')!!}
    </div>
    @if(Auth::check() and (Auth::user()->isAdmin() or Auth::user()->id == $player->id))
        <div class="content_dark">
            <div class="content_inner">
                <div class="boxes_wrap white_links">
                    <span class="headline_lw" style="font-weight: bold">
                        {!! trans('localization.admin_section')!!}
                    </span>
                    <br>
                    <br>
                    <form method="POST" action="/players/{{ $player->id }}" class="post-form admin-form">
                        @csrf
                        {{method_field('PATCH')}}
                        <div class="form-group">
                            <label for="name">{!! trans('localization.name')!!}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$player->name}}">
                        </div>
                        <div class="form-group">
                            <label for="surname">{!! trans('localization.surname')!!}</label>
                            <input type="text" class="form-control" id="surname" name="surname" value="{{$player->surname}}">
                        </div>
                        <div class="form-group">
                            <label for="email">{!! trans('localization.email')!!}</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$player->email}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="button_submit">
                                <span class="button_link_medium button_admin">
                                    {!! trans('localization.save_information')!!}
                                </span>
                            </button>
                        </div>
                    </form>
                    <form method="POST" action="/players/{{ $player->id }}/password" class="post-form admin-form">
                        @csrf
                        {{method_field('PATCH')}}
                        <div class="form-group">
                            <label for="current-password">{!! trans('localization.current_password')!!}:</label>
                            <input type="password" class="form-control" id="current-password" name="current-password">
                        </div>
                        <div class="form-group">
                            <label for="password">{!! trans('localization.new_password')!!}:</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{!! trans('localization.password_confirmation')!!}:</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="button_submit">
                                <span class="button_link_medium button_admin">
                                    {!! trans('localization.change_password')!!}
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <span class="headline_lw">
                {!! trans('localization.information')!!}
        </span>
        <p>
            {!! trans('localization.number_matches_played')!!}: {{$player->singlesTeam()->matches->count()}}
            <br>
            {!!trans('localization.elo')!!}: {{$player->singlesTeam()->getFormattedWinRatio()}}
            <br>
            {!! trans('localization.name')!!}: {{ $player -> name }}
            <br>
            {!! trans('localization.surname')!!}: {{ $player -> surname }}
            <br>
            {!! trans('localization.email')!!}: {{ $player -> email }}
        </p>

        <span class="headline_lw">
                {!! trans('localization.matches_to_be_played')!!}
        </span>
        @foreach($player->teams as $team)
            <ul>
                @foreach($team->matches as $match)
                    @if(!$match->played)
                        <li><a href="/matches/{{ $match->id }}">{{$match->buildName()}}</a></li>
                    @endif
                @endforeach
            </ul>
        @endforeach

{{--        TODO only for SINGLES--}}
        <span class="headline_lw">
                {!! trans('localization.match_history')!!}
        </span>
        <table style="text-align: center;">
            <thead>
                <tr>
                    <th>{!! trans('localization.player')!!}</th>
                    <th>{!! trans('localization.opponent')!!}</th>
                    <th>{!! trans('localization.match_result')!!}</th>
                    <th>{!! trans('localization.tournament')!!}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($player->singlesTeam()->matches->sortByDesc('created_at') as $match)
                <tr>
                    <td>
                        <a href="/players/{{ $player->id }}">{{$player->getFullNameAttribute()}}</a>
                        <br>
                        {{$player->singlesTeam()->getFormattedWinRatio()}}
                    </td>
                    <td>
                        @if($match->teamFirst->playerFirst->id == $player->id)
                            <a href="/players/{{ $match->teamSecond->playerFirst->id }}">{{$match->teamSecondName()}}</a>
                            <br>
                            {{$match->teamSecond->getFormattedWinRatio()}}
                        @else
                            <a href="/players/{{ $match->teamFirst->playerFirst->id }}">{{$match->teamFirstName()}}</a>
                            <br>
                            {{$match->teamFirst->getFormattedWinRatio()}}
                        @endif
                    </td>
                    <td >
                        @if($match->teamFirst->playerFirst->id == $player->id)
                            {{$match->buildResult()}}
                        @else
                            {{$match->buildReverseResult()}}
                        @endif
                    </td>
                    <td>
                        <a href="/tournaments/{{ $match->tournament()->id }}">
                            {{$match->tournament()->tournament_number}}. {!! trans('localization.tournament_lowercase')!!}
                        </a>
                        <br>
                        <a href="/semesters/{{ $match->tournament()->semester->id }}">
                            {{$match->tournament()->semester->semester_name}}
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


{{--        TODO doubles--}}
        @if(Auth::user()->isAdmin())
{{--        remove if admin--}}
            @if($player->teams()->count()>0)
                <span class="headline_lw">
                        {!! trans('localization.teams')!!}
                </span>
            @endif
            <div id="players-teams">
                @foreach( $player -> teams as $team )
                    @if(!$team->active)
                        <a href="/teams/{{ $team->id }}" class="text-info">{{ $team -> team_name }}</a>
                        <p style="display: inline" class="text-danger text-uppercase">
                            {!! trans('localization.inactive')!!}
                        </p>
                    @else
                        <a href="/teams/{{ $team->id }}" class="text-justify">{{ $team -> team_name }}</a>
                    @endif
                @endforeach
            </div>
        @endif
    </div>
    @include('partials.errors')
@endsection