@extends('layouts.master')

@section('content')
<div class="content_headline">
	{!! trans('localization.nav_tournaments')!!}
</div>
@if(Auth::check() and Auth::user()->isAdmin())
<div class="content_dark">
	<div class="content_inner">
		<div class="boxes_wrap white_links">
			<span class="headline_lw" style="font-weight: bold">
				{!! trans('localization.admin_section')!!}
			</span>
			<br>
			<br>
			<form method="POST" action="/semesters" class="post-form admin-form">
				@csrf
				<div class="form-group">
					<label for="semester_season">
						{!! trans('localization.semester_season')!!}
					</label>
					<br>
					<select class="form-control js-example-basic-single" id="semester_season" name="semester_season">
						<option disabled selected>{!! trans('localization.select_option')!!}</option>
						<option value="ZS">ZS</option>
						<option value="LS">LS</option>
					</select>
					<br>
					<br>
					<label for="semester_year">
						{!! trans('localization.semester_year')!!}
					</label>
					<br>
					<select class="form-control js-example-basic-single" id="semester_year" name="semester_year">
						<option disabled selected>
							{!! trans('localization.select_option')!!}
						</option>
						@for($year = 2010; $year <= 2030; $year++)
							<option value="{{$year}}/{{$year+1}}">
								{{$year}}/{{$year+1}}
							</option>
						@endfor
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="button_submit">
						<span class="button_link_medium button_admin">
							{!! trans('localization.create_semester')!!}
						</span>
					</button>
				</div>
			</form>
		</div>
		<br>
	</div>
</div>
@endif
<div class="content white_links">
	<div class="content_inner">
		<br>
		<div class="boxes_wrap">
            <span class="headline_lw">
                {!! trans('localization.current')!!}
            </span>
            <br>
            <br>
			@if($activeSemester)
				<a href="/semesters/{{$activeSemester->id}}" class="semester_link">
				<span class="button_link_large">
					{!! trans('localization.current')!!}
				</span>
				</a>
			@else
				<p style="margin: 0 auto">
					{!! trans('localization.current_not_found')!!}
				</p>
			@endif
		</div>
		<br>
		<div class="boxes_wrap">
            <span class="headline_lw">
                {!! trans('localization.previous')!!}
            </span>
            <br>
            <br>
            @foreach(
            	$semesters
            	->sort(function($a, $b){return strcmp(substr($b->semester_name, 2), substr($a->semester_name,2)) ?: strcmp(substr($a->semester_name, 0, 2), substr($b->semester_name, 0, 2));})
			 as $semester)
                <a href="/semesters/{{ $semester->id }}" class="semester_link">
                <span class="button_link_large">
                    {{ $semester->semester_name }}
                </span>
                </a>
            @endforeach
		</div>
		<br>
	</div>
</div>

    @include('partials.errors')

@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			$('.js-example-basic-single').select2();
		});
	</script>
@endsection