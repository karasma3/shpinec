@extends('layouts.master')

@section('content')
  <div class="content_headline">
    {!! trans('localization.semester')!!}: {{ $semester -> semester_name }}
  </div>
  @if(Auth::check() and Auth::user()->isAdmin())
    <div class="content_dark">
      <div class="content_inner">
        <div class="boxes_wrap white_links">
					<span class="headline_lw" style="font-weight: bold">
						{!! trans('localization.admin_section')!!}
					</span>
          <br>
          <br>
          <a href="mailto:{{$semester->getEmails()}}" class="button_link">
						<span class="button_link_medium button_admin">
							{!! trans('localization.send_email_to_participants')!!}
						</span>
          </a>
          <form method="POST" action="/semesters/{{$semester -> id}}/setActive" class="post-form admin-form">
            @csrf
            <button type="submit" class="button_submit">
							<span class="button_link_medium button_admin">
								@if($semester->active)
                  {!! trans('localization.deactivate')!!}
                @else
                  {!! trans('localization.activate')!!}
                @endif
							</span>
            </button>
          </form>
          @if(!$semester->hasFinalTournament() && $semester->active)
            <form method="POST" action="/semesters/{{$semester->id}}/addTournament" class="post-form admin-form">
              @csrf
              <div class="form-group">
                <input type="checkbox" id="final_tournament" name="final_tournament" value="true">
                <label for="final_tournament">
                  {!! trans('localization.final_tournament')!!}
                </label>
              </div>
              <div class="form-group">
                <button type="submit" class="button_submit">
									<span class="button_link_medium button_admin">
										{!! trans('localization.create_tournament')!!}
									</span>
                </button>
              </div>
            </form>
          @endif
          {{-- this is delete button --}}
          {{--
          <form method="POST" action="/semesters/{{$semester -> id}}/delete" class="post-form admin-form">
            @csrf
            @method('DELETE')
            <button type="submit" class="button_submit">
              <span class="button_link_medium button_admin">
                {!! trans('localization.semester_delete')!!}
              </span>
            </button>
          </form>
          --}}
        </div>
        <br>
      </div>
    </div>
  @endif
  <div class="content">
    <div class="content_inner">
      <div class="boxes_wrap">
				<span class="headline_lw">
						{!! trans('localization.quick_navigation')!!}
				</span>
        <br>
        <a href="/semesters" class="button_link">
					<span class="button_link_small">
						{!! trans('localization.back_to_semesters')!!}
					</span>
        </a>
        <span class="headline_lw">
					{!! trans('localization.tournaments')!!}
				</span>
        <br>
        <br>

        <div class="table">
          <div class="table-head">
            <div class="table-col">
              {!!trans('localization.tournament')!!}
            </div>
            <div class="table-col">
              {!! trans('localization.date') !!}
            </div>
			<div class="table-col">
              {!! trans('localization.locality') !!}
            </div>
            <div class="table-col">
              {!! trans('localization.registered_players') !!}
            </div>
			<div class="table-col">
			  {!! trans('localization.register') !!}
			</div>
          </div>
          <div class="table-body">
            @foreach($semester -> tournaments->sortBy('tournament_number') as $tournament)
              <a href="/tournaments/{{$tournament->id}}" class="table-row-a" title="{{$tournament->getTournamentName()}}">
                <div class="table-row">
                  <div class="table-col" data-title="{!!trans('localization.tournament')!!}">
                    @if($tournament->final_tournament)
                      {!! trans('localization.final_tournament')!!}
                    @else
                      {{$tournament->tournament_number}}. {!! trans('localization.tournament_lowercase')!!}
                    @endif
                  </div>
                  <div class="table-col" data-title="{!! trans('localization.date') !!}">
                    {{date('d.m.Y H:i', strtotime($tournament->tournament_date))}}
                  </div>
				  <div class="table-col" data-title="{!! trans('localization.locality') !!}">
					  {{$tournament->tournament_locality}}
                  </div>
                  <div class="table-col" data-title="{!! trans('localization.registered_players') !!}">
                    {{ $tournament->teams->count()}} / {{\App\Models\Tournament::MAX_PLAYER_COUNT}}
                  </div>
				  <div class="table-col" data-title="{!! trans('localization.register') !!}">
					@if($tournament->isCreated())
                        @if(Auth::check())
                           @if(!$tournament->teams()->find(Auth::user()->singlesTeam()->id))
                              <form method="POST" action="/tournaments/{{$tournament->id}}/join" class="table-col-a">
                                  @csrf
                                  <div class="form-group">
                                      <select class="form-control js-example-basic-single"  id="team_id" name="team_id" hidden="true">
                                          <option value="{{ Auth::user()->singlesTeam()->id }}" selected></option>
                                      </select>
                                  </div>
                                  <button type="submit" class="button_submit register">
                                      <img src="{!! asset("img/enter-ico.png") !!}" alt="{!! trans('localization.register') !!}" title="{!! trans('localization.register') !!}"/>
                                  </button>
                              </form>
                           @else
                              <form method="POST" action="/tournaments/{{$tournament->id}}/removeTeam" class="table-col-a">
                                  @csrf
                                  <div class="form-group">
                                      <input type="hidden" class="form-control" id="team_id" name="team_id" value={{ Auth::user()->singlesTeam()->id }}>
                                  </div>
                                  <div class="form-group">
                                      <button type="submit" class="button_submit register">
                                          <img src="{!! asset("img/logout-ico.png") !!}" alt="{!! trans('localization.remove_team') !!}" title="{!! trans('localization.remove_team') !!}"/>
                                      </button>
                                  </div>
                              </form>
                           @endif
                        @else
                          <object>
                              <a href="/login" class="table-col-a register">
                                  <img src="{!! asset("img/enter-ico.png") !!}" alt="{!! trans('localization.nav_login') !!}" title="{!! trans('localization.nav_login') !!}"/>
                              </a>
                          </object>
                        @endif
					@else
                        <div  class="table-col-a register">
                            <img src="{!! asset("img/cancel-ico.png") !!}" alt="{!! trans('localization.registration_closed') !!}" title="{!! trans('localization.registration_closed') !!}"/>
                        </div>
					@endif
				  </div>
                </div>
              </a>
            @endforeach
          </div>
        </div>
      </div>
      <br>

      @if($semester->hasFinalTournament() && $semester->getFinalTournament()->isClosed())
        <span class="headline_lw">
					{!! trans('localization.final_tournament_standings')!!}
					<a href="{{action('TournamentController@export', $semester->getFinalTournament()->id)}}">
						<img src="<?php echo asset("img/download_file_logo.PNG")?>" style="height: 20px; float: right;"
                 alt="Download logo">
					</a>
					<span style="float: right; font-size: 14px;">export&nbsp;</span>
				</span>
        <br>
        <br>
        @include('partials.standings', ['standingsData' => $semester->getFinalTournament()->getStandingsTable()])
      @endif
      <br>
      <span class="headline_lw">
				{!! trans('localization.semester_standings')!!}
				<a href="{{action('SemesterController@export', $semester->id)}}">
					<img src="<?php echo asset("img/download_file_logo.PNG")?>" style="height: 20px; float: right;"
               alt="Download logo">
				</a>
			<span style="float: right; font-size: 14px;">export&nbsp;</span>
		</span>
      <br>
      <br>
      @include('partials.standings', ['standingsData' => $standingsTable])
      @include('partials.errors')
    </div>
  </div>
@endsection
