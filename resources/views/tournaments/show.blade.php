@extends('layouts.master')

@section('head')
	@if($tournament->isCreated())
		{{--    datetime picker--}}
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	@endif
@endsection

@section('content')
	<div class="content_headline">
		<span style="position: absolute; left: 20px; font-size: 18px;">
			{!! trans('localization.registered_players')!!}: {{$tournament->teams->count()}}
		</span>
        {{ $tournament -> tournament_number }}. {!! trans('localization.tournament_lowercase')!!} - {{$tournament->semester->semester_name}}
		<span style="position: absolute; right: 20px; font-size: 18px;">
			{!! trans('localization.state')!!}: {{$tournament->getState()}}
		</span>
	</div>
	@if(Auth::check())

		<div class="content_dark">
			<div class="content_inner">
				@if(Auth::user()->isAdmin())
                    <div class="boxes_wrap white_links">
                        <span class="headline_lw" style="font-weight: bold">
                            {!! trans('localization.admin_section')!!}
                        </span>
                        <br>
                        <br>
                        @if($tournament->isCreated())
{{--							set tournament date--}}
							<form method="POST" action="/tournaments/{{ $tournament->id }}/save_info" class="post-form admin-form">
								@csrf
								<label for="from">
									{!! trans('localization.date')!!}
								</label>
								<div class="form-group input-group date" id="datetimepicker2" style="max-width: 195px">
									<input type="text" class="form-control" value="" id="from" name="from" required>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
                                <div>
                                    <label for="locality">
                                        {!! trans('localization.locality')!!}
                                    </label>
                                    <input type="text" class="form-control" value="{{$tournament->tournament_locality}}" id="locality" name="locality" required>
                                </div>
                                <br>
								<button type="submit" class="button_submit">
										<span class="button_link_medium button_admin">
											{!! trans('localization.save')!!}
										</span>
								</button>
							</form>

							<form method="POST" action="/tournaments/{{ $tournament->id }}/generate_groups" class="post-form admin-form">
								@csrf
								<button type="submit" class="button_submit">
                                    <span class="button_link_medium button_admin">
                                        {!! trans('localization.generate_groups')!!}
                                    </span>
								</button>
							</form>
						@endif
                        @if($tournament->isGroupStage())
                            <form method="POST" action="/tournaments/{{ $tournament->id }}/calculate_score" class="post-form admin-form">
                                @csrf
                                {{method_field('PATCH')}}
                                <button type="submit" class="button_submit">
                                    <span class="button_link_medium button_admin">
                                        {!! trans('localization.calculate_score')!!}
                                    </span>
                                </button>
                            </form>
                            <form method="POST" action="/tournaments/{{ $tournament->id }}/create_bracket" class="post-form admin-form">
                                @csrf
                                <button type="submit" class="button_submit">
                                    <span class="button_link_medium button_admin">
                                        {!! trans('localization.create_bracket')!!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        @if($tournament->isEliminationStage())
                            <form method="POST" action="/tournaments/{{ $tournament->id }}/next_round" class="post-form admin-form">
                                @csrf
                                {{method_field('PATCH')}}
                                <button type="submit" class="button_submit">
                                    <span class="button_link_medium button_admin">
                                        {!! trans('localization.next_round')!!}
                                    </span>
                                </button>
                            </form>
                            <form method="POST" action="/tournaments/{{ $tournament->id }}/close" class="post-form admin-form">
                                @csrf
                                <button type="submit" class="button_submit">
                                    <span class="button_link_medium button_admin">
                                        {!! trans('localization.close_tournament')!!}
                                    </span>
                                </button>
                            </form>
                        @endif
                        <a href="mailto:{{$tournament->getEmails()}}" class="button_link">
                            <span class="button_link_medium button_admin">
                                {!! trans('localization.send_email_to_participants')!!}
                            </span>
                        </a>
                        @if($tournament->isEliminationStage())
                            <a href="mailto:{{$tournament->getEmailsElimination()}}" class="button_link">
                                <span class="button_link_medium button_admin">
                                    {!! trans('localization.send_email_elimination_stage')!!}
                                </span>
                            </a>
                        @endif
    {{--					manualne vytvaranie skupiny a zapasov, nastavovanie dalsej fazy--}}
    {{--					--}}
    {{--					@if($tournament->isCreated())--}}
    {{--						<form method="POST" action="/tournaments/{{ $tournament->id }}/create_groups" class="post-form admin-form">--}}
    {{--							@csrf--}}
    {{--							<button type="submit" class="button_submit">--}}
    {{--								<span class="button_link_medium button_admin">--}}
    {{--									{!! trans('localization.create_groups')!!}--}}
    {{--								</span>--}}
    {{--							</button>--}}
    {{--						</form>--}}
    {{--						<form method="POST" action="/tournaments/{{ $tournament->id }}/create_matches" class="post-form admin-form">--}}
    {{--							@csrf--}}
    {{--							<button type="submit" class="button_submit">--}}
    {{--								<span class="button_link_medium button_admin">--}}
    {{--									{!! trans('localization.create_matches')!!}--}}
    {{--								</span>--}}
    {{--							</button>--}}
    {{--						</form>--}}
    {{--						<form method="POST" action="/tournaments/{{ $tournament->id }}/set_next_state" class="post-form admin-form">--}}
    {{--							@csrf--}}
    {{--							<button type="submit" class="button_submit">--}}
    {{--								<span class="button_link_medium button_admin">--}}
    {{--									{!! trans('localization.set_next_state')!!}--}}
    {{--								</span>--}}
    {{--							</button>--}}
    {{--						</form>--}}
    {{--					@endif--}}
                    </div>
				@endif
                @if($tournament->isCreated() and Auth::check())
                    <div class="boxes_wrap white_links">
                    {{--registracny panel--}}
                    <span class="headline_lw" style="font-weight: bold">
                        {!! trans('localization.registration')!!}
					</span>
                    <br>
                    {{--					--}}
						<form method="POST" action="/tournaments/{{$tournament->id}}/join" class="post-form admin-form">
							@csrf
							<div class="form-group">
								<label for="team_id">
									{!! trans('localization.team')!!}
								</label>
								<br>
								<select class="form-control js-example-basic-single"  id="team_id" name="team_id">
									@if(Auth::user()->isOrganizer())
										<option disabled selected>
											{!! trans('localization.select_option')!!}
										</option>
										@foreach( \App\Models\Team::all()->sortBy('team_name', SORT_NATURAL|SORT_FLAG_CASE) as $team)
											<option value="{{ $team->id }}">
												{{ $team->team_name }} - {{ substr($team->getEmails(),0,-1) }}
											</option>
										@endforeach
									@else
										@foreach( auth()->user()->teams->sortBy('team_name') as $team)
											<option value="{{ $team->id }}" selected>
												{{ $team->team_name }}
											</option>
										@endforeach
									@endif
								</select>
							</div>
							<button type="submit" class="button_submit">
									<span class="button_link_medium button_admin">
										{!! trans('localization.register')!!}
									</span>
							</button>
						</form>
						@if(Auth::check() and Auth::user()->isOrganizer())
						{{--registruj este neexistujuceho usera--}}
						{{----}}
						<form method="POST" action="/register/join/{{$tournament->id}}" class="post-form admin-form">
							@csrf
							<div class="form-group">
								<label for="name">
									{!! trans('localization.name')!!}
								</label>
								<input type="text" class="form-control" id="name" name="name" required>
							</div>
							<div class="form-group">
								<label for="surname">
									{!! trans('localization.surname')!!}
								</label>
								<input type="text" class="form-control" id="surname" name="surname" required>
							</div>
							<div class="form-group">
								<label for="email">
									{!! trans('localization.email')!!}
								</label>
								<input type="email" class="form-control" id="email" name="email" required>
							</div>
							{{--heslo admin natvrdo--}}
							<div class="form-group">
								<input type="hidden" id="password" name="password" value="admin">
							</div>
							<div class="form-group">
								<input type="hidden" id="password_confirmation" name="password_confirmation" value="admin">
							</div>
							{{--							--}}
							<div class="form-group">
								<button type="submit" class="button_submit">
										<span class="button_link_medium button_admin">
											{!! trans('localization.register')!!}
										</span>
								</button>
							</div>
						</form>
						{{--						--}}
						@endif
                    </div>
                @endif
				<br>
			</div>
		</div>

	@endif

    <div class="content">
		<div class="content_inner">
			<span class="headline_lw">
				{!! trans('localization.quick_navigation')!!}
			</span>
			<br>
			<div class="boxes_wrap white_links">
				@if($tournament->isEliminationStage() || $tournament->isClosed())
					<a href="#Bracket" class="button_link">
						<span class="button_link_small">
							{!! trans('localization.bracket')!!}
						</span>
					</a>
					@if(!$tournament->loserBrackets->isEmpty())
						<a href="/loserBracket/{{ $tournament->id }}" class="button_link">
							<span class="button_link_small">
								{!! trans('localization.loser_bracket')!!}
							</span>
						</a>
					@endif
				@endif
				@if(!$tournament->isCreated())
					<a href="#Groups" class="button_link">
						<span class="button_link_small">
							{!! trans('localization.groups')!!}
						</span>
					</a>
				@endif
				{{-- <!--
				<a href="/tournaments" class="button_link">
				<span class="button_link_small">
					{!! trans('localization.back')!!}
				</span>
				</a>
				--> --}}
				<a href="/semesters/{{ $tournament->semester->id }}" class="button_link">
				<span class="button_link_small">
					{!! trans('localization.back_to_semester')!!}
				</span>
				</a>
			</div>
			<br>
			@if($tournament->isCreated() || $tournament->isClosed())
				<span class="headline_lw">
						{!! trans('localization.information')!!}
					</span>
				<br>
				<table style="table-layout: fixed;width: 100%;text-align: center; font-size: xx-large">
					<tbody>
					<tr>
						<td>{!! trans('localization.date')!!}:</td>
						<td>{{$tournament->getFormattedTournamentDate()}}</td>
					</tr>
					<tr>
						<td>{!! trans('localization.locality')!!}:</td>
						<td>{{$tournament->tournament_locality}}</td>
					</tr>
					</tbody>
				</table>
				<br>
			@endif
			@if($tournament->isCreated())
                <span class="headline_lw">
					{!! trans('localization.registered_teams')!!}
				</span>
				<br>
				<ul>
					@foreach($tournament->teams as $team)
						<li>
{{--							TODO doubles tournament link for teams--}}
{{--							<a href="/teams/{{ $team->id }}">{{ $team->team_name }}</a>--}}
							<a href="/players/{{ $team->player_id_first }}">{{ $team->team_name }}</a>
							@if(Auth::check() and (Auth::user()->isOrganizer() || Auth::user()->singlesTeam()->id == $team->id))
								<form method="POST" action="/tournaments/{{$tournament->id}}/removeTeam" class="post-form admin-form">
									@csrf
									<div class="form-group">
										<input type="hidden" class="form-control" id="team_id" name="team_id" value={{$team->id}}>
									</div>
									<div class="form-group">
										<button type="submit" class="button_submit">
											<span class="button_link_small button_admin">
												{!! trans('localization.remove_team')!!}
											</span>
										</button>
									</div>
								</form>
							@endif
						</li>
					@endforeach
				</ul>
				<br>
			@endif
			@if($tournament->isClosed())
				<span class="headline_lw">
					{!! trans('localization.tournament_standings')!!}
					<a href="{{action('TournamentController@export', $tournament->id)}}">
						<img src="<?php echo asset("img/download_file_logo.PNG")?>" style="height: 20px; float: right;" alt="Download logo">

					</a>
					<span style="float: right; font-size: 14px;">
						export&nbsp;
					</span>
				</span>
				<br>
				<br>
				@include('partials.standings', ['standingsData' => $standingsTable, 'hideAdvancingPlayers' => true])
				<br>
			@endif

        @if($tournament->isEliminationStage() || $tournament->isClosed())
			<span class="headline_lw" id="Bracket">
				{!! trans('localization.bracket')!!}
			</span>
			<br>
			<br>
            @include('partials.bracket', ['brackets' => $tournament->brackets, 'firstRoundBracket' => $tournament->firstRoundBracket])

            <br>
            <br>
            @if($tournament->isEliminationStage())
				<span class="headline_lw">
					{!! trans('localization.teams')!!}
				</span>
				<ul>
				@foreach($tournament->teamsInEliminationStage as $team)
						<li>
{{--							TODO doubles tournament link for teams--}}
{{--							<a href="/teams/{{$team->id}}">{{$team->team_name}}</a>--}}
							<a href="/players/{{$team->player_id_first}}">{{$team->team_name}}</a>
						</li>
				@endforeach
				</ul>
				@foreach($tournament->brackets as $bracket)
					<span class="headline_lw">
						{!! trans('localization.round')!!}: {{$bracket->round}}
					</span>
					<h5>{!! trans('localization.matches')!!}</h5>
					<ul>
						@foreach($bracket->matchesInOrder as $match)
							@if(!is_null($match->teamSecondName()))
								<li><a href="/matches/{{ $match->id }}">{{ $match->buildName() }}</a> <p style="display: inline">{{$match->buildResult()}}</p></li>
							@endif
						@endforeach
					</ul>
				@endforeach
            @endif
        @endif
        @if($tournament->isGroupStage() || $tournament->isClosed())
			<br>
			<div class="content_inner" id="Groups">
				<span class="headline_lw">
					{!! trans('localization.groups')!!}
				</span>
				<br>
				<br>
				@foreach($tournament->groups as $group)
					@include('partials.table')
				@endforeach
			</div>
        @endif
		</div><!--content_inner-->
	</div>
    @include('partials.errors')
@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			$('.js-example-basic-single').select2();
		});
	</script>

    @if($tournament->isCreated())
        {{--	<script src="/js/jquery.min.js"></script>--}}
		<script src="/js/moment-with-locales.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/bootstrap-datetimepicker.min.js"></script>

		<script type="text/javascript">
			$(function () {
				$('#datetimepicker2').datetimepicker({
					locale: 'cs'
				});
                $('#from').val('{{$tournament->getFormattedTournamentDate()}}');
			});
		</script>
	@endif
@endsection