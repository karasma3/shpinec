@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {{ $tournament -> tournament_number }}. {!! trans('localization.tournament_lowercase')!!} - {{$tournament->semester->semester_name}}
        <span style="position: absolute; right: 20px; font-size: 18px;">
        {!! trans('localization.state')!!}: {{$tournament->getState()}}
        </span>
    </div>

    @if(Auth::check() and Auth::user()->isAdmin())
        <div class="content_dark">
            <div class="content_inner">
                <div class="boxes_wrap white_links">
                <span class="headline_lw" style="font-weight: bold">
                    {!! trans('localization.admin_section')!!}
                </span>
                <br>
                <br>
                @if($tournament->isEliminationStage())
                    <form method="POST" action="/tournaments/{{ $tournament->id }}/nextLoserBracket" class="post-form admin-form">
                        @csrf
                        {{method_field('PATCH')}}
                        <button type="submit" class="button_submit">
                            <span class="button_link_medium button_admin">
                                {!! trans('localization.next_round')!!}
                            </span>
                        </button>
                    </form>
                @endif
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <span class="headline_lw">
            {!! trans('localization.quick_navigation')!!}
        </span>
        <br>
        <div class="boxes_wrap white_links">
            <a href="/tournaments/{{ $tournament->id }}" class="button_link">
                <span class="button_link_small">
                    {!! trans('localization.back_to_tournament')!!}
                </span>
            </a>
        </div>


        <span class="headline_lw" id="LoserBracket">
            {!! trans('localization.loser_bracket')!!}
        </span>
        <br>
        <br>
        @include('partials.bracket', ['brackets' => $tournament->loserBrackets, 'firstRoundBracket' => $tournament->firstRoundLoserBracket])

    </div>
    @include('partials.errors')
@endsection