@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.match')!!}: {{$match->teamFirst->team_name}} vs {{$match->teamSecond->team_name}}
    </div>

    <div class="content">
        <div class="content_inner">
            <span class="headline_lw">
                {!! trans('localization.quick_navigation')!!}
            </span>
            <br>
            <div class="boxes_wrap white_links">
                @if($match->group->isLoserBracket())
                    <a href="/loserBracket/{{ $match->group->tournament->id }}" class="button_link">
                @else
                    <a href="/tournaments/{{ $match->group->tournament->id }}" class="button_link">
                @endif
            <span class="button_link_small">
                {!! trans('localization.back_to_tournament')!!}
            </span>
                </a>
            </div>
            <br>
{{--							TODO doubles tournament link for teams--}}
{{--            <a href="/teams/{{$match->team_id_first}}">{{$match->teamFirst->team_name}}</a> vs <a href="/teams/{{$match->team_id_second}}">{{$match->teamSecond->team_name}}</a>--}}
{{--            <a href="/players/{{$match->teamFirst()->first()->player_id_first}}">{{$match->teamFirst->team_name}}</a> vs <a href="/players/{{$match->teamSecond()->first()->player_id_first}}">{{$match->teamSecond->team_name}}</a>--}}
{{--            <div class="form-group">--}}
{{--                {!! trans('localization.match_result')!!}: {{ $match->buildResult() }}--}}
{{--            </div>--}}


            <form method="POST" action="/matches/{{ $match->id }}">
                @csrf
                {{method_field('PATCH')}}
                <table style="table-layout: fixed;width: 100%;text-align: center">
                    <thead>
                        <tr>
                            <th>
                                <a href="/players/{{$match->teamFirst()->first()->player_id_first}}">
                                    {{$match->teamFirstName()}}
                                </a>
                            </th>

                            @if(Auth::check() and (Auth::user()->isOrganizer() or (Auth::user()->participant($match) and !$match->tournament()->isClosed())))
                                <th style="display: flex">
                                    <select class="form-control" id="score_first" name="score_first">
                                        @for($value = 0; $value <= 4; $value++)
                                            <option value="{{$value}}" @if ($value == $match->scoreFirst()) selected @endif>
                                                {{$value}}
                                            </option>
                                        @endfor
                                    </select>
                                    :
                                    <select class="form-control" id="score_second" name="score_second">
                                        @for($value = 0; $value <= 4; $value++)
                                            <option value="{{$value}}" @if ($value == $match->scoreSecond()) selected @endif>
                                                {{$value}}
                                            </option>
                                        @endfor
                                    </select>
                                </th>
                            @else
                                <th>
                                    {{$match->buildResult()}}
                                </th>
                            @endif
                            <th>
                                <a href="/players/{{$match->teamSecond()->first()->player_id_first}}">
                                    {{$match->teamSecondName()}}
                                </a>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>
                                {{$match->teamFirst->getFormattedWinRatio()}}
                            </td>
                            <td>
                                @if(Auth::check() and (Auth::user()->isOrganizer() or (Auth::user()->participant($match) and !$match->tournament()->isClosed())))
                                    <button type="submit" class="button_submit button_link">
                                        <span class="button_link_small button_admin">
                                            {!! trans('localization.match_edit')!!}
                                        </span>
                                    </button>
                                @endif
                            </td>
                            <td>
                                {{$match->teamSecond->getFormattedWinRatio()}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    @include('partials.errors')
@endsection