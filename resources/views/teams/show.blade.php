@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.team')!!}: {{ $team->team_name}}
    </div>
    @if(Auth::check() and (Auth::user()->isAdmin() or Auth::user()->id == $team->player_id_first or Auth::user()->id == $team->player_id_second))
        <div class="content_dark">
            <div class="content_inner">
                <div class="boxes_wrap white_links">
                    <span class="headline_lw" style="font-weight: bold">
                        {!! trans('localization.admin_section')!!}
                    </span>
                    <br>
                    <br>
{{--                    tlacitko na deaktivovanie timu--}}
{{--                    --}}
{{--                    <form method="POST" action="/teams/{{ $team->id }}/inactivate" class="post-form admin-form">--}}
{{--                        @csrf--}}
{{--                        <button type="submit" class="button_submit">--}}
{{--                            <span class="button_link_medium button_admin">--}}
{{--                                {!! trans('localization.deactivate')!!}--}}
{{--                            </span>--}}
{{--                        </button>--}}
{{--                    </form>--}}
                </div>
            </div>
        </div>
    @endif
    <div class="content">
        <div class="content_inner">
				<span class="headline_lw">
						{!! trans('localization.players')!!}
				</span>
                <br>
                <ul>
                    <li>
                        <a href="/players/{{ $team->player_id_first }}">
                            {{ $team->playerFirstFullName() }}
                        </a>
                    </li>
                    @if($team->player_id_second)
                        <li>
                            <a href="/players/{{ $team->player_id_second }}">
                                {{ $team->playerSecondFullName() }}
                            </a>
                        </li>
                    @endif
                </ul>
        </div>
    </div>
    @include('partials.errors')
@endsection