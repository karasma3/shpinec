@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.doubles_create')!!}
    </div>
    <div class="content">
        <div class="content_inner">
            <form method="POST" action="/teams" class="post-form">
                @csrf
                <div class="form-group">
                    <label for="email_first">
                        {!! trans('localization.doubles_email_first')!!}
                    </label>
                    <input type="text" class="form-control" id="email_first" name="email_first">
                    <br>
                    <label for="email_second">
                        {!! trans('localization.doubles_email_second')!!}
                    </label>
                    <input type="text" class="form-control" id="email_second" name="email_second">
                </div>
                <div class="form-group">
                    <button type="submit" class="button_submit">
                        <span class="button_link_medium">
                            {!! trans('localization.doubles_create')!!}
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    @include('partials.errors')
@endsection