@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.registration')!!}
    </div>
    <br>
    <br>
    <form method="POST" action="/register" class="post-form">
        @csrf
            <div class="form-group">
                <label for="name">
                    {!! trans('localization.name')!!}
                </label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="surname">
                    {!! trans('localization.surname')!!}
                </label>
                <input type="text" class="form-control" id="surname" name="surname" required>
            </div>
            <div class="form-group">
                <label for="email">
                    {!! trans('localization.email')!!}
                </label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">
                    {!! trans('localization.password')!!}
                </label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <div class="form-group">
                <label for="password_confirmation">
                    {!! trans('localization.password_confirmation')!!}
                </label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
            </div>
            <div class="form-group">
                <button type="submit" class="button_submit">
                    <span class="button_link_small">
                        {!! trans('localization.register')!!}
                    </span>
                </button>
            </div>
    </form>
    @include('partials.errors')
@endsection
