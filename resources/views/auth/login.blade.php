@extends('layouts.master')

@section('content')
    <div class="content_headline">
        {!! trans('localization.login')!!}
    </div>
    <br>
    <br>
    <form method="POST" action="/login" class="post-form">
        @csrf
        <div class="form-group">
            <label for="email">
                {!! trans('localization.email')!!}
            </label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="password">
                {!! trans('localization.password')!!}
            </label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        <div class="form-group">
            <button type="submit" class="button_submit button_link">
                <span class="button_link_small">
                    {!! trans('localization.nav_login')!!}
                </span>
            </button>
        </div>
    </form>
    @include('partials.errors')
@endsection
