@extends('layouts.master')

@section('content')
    <img src="<?php echo asset("img/Pinec.png")?>"
         style="display: block; width: 100%; min-width: 300px; max-width: 1850px; margin: 0 auto;" alt="Logo SH Pinec">
    <!--STRAHOVSKA LIGA-->
    <div class="content_headline">
        {!! trans('localization.shpinec_headline')!!}
    </div>
	<div class="content">
	<div class="content_inner">
		<br><br>
		<p class="content_p c">
			{!! trans('localization.introduction_1')!!}
		</p>
		<p class="content_p c">
			{!! trans('localization.introduction_2')!!}
			{!! trans('localization.introduction_3')!!}
		</p>
		<p class="content_p c">
			<a href="http://wiki.siliconhill.cz/SH_liga_ve_stoln%C3%ADm_tenise">Sillicon Hill Wiki</a>
		</p>
		<br><br>
	</div>
	</div>
    <div class="clear"></div>



    <!--INFORMACE-->
    <div class="content_headline" style="margin-top: -25px;">
        {!! trans('localization.info_headline')!!}
    </div>
    <div class="content_dark" id="info">
        <a id="info" style="display: inline-block; padding-top: 150px; margin-top:-150px">&nbsp;</a>
        <br><br>
		<div class="content_inner">
			<span class="headline_lw">
				{!! trans('localization.info_system')!!}
			</span>
			<br>
			<p class="content_p bl">
				{!! trans('localization.info_system_1')!!}
			</p>
			<br>
			<span class="headline_lw">
				{!! trans('localization.info_rules')!!}
			</span>
			<div class="content_ul">
				{!! trans('localization.info_rules_1')!!}
			</div>
			<br>
			<span class="headline_lw">
				{!! trans('localization.info_points')!!}
			</span>
			<div class="content_ul">
				{!! trans('localization.info_points_1')!!}
			</div>
			<br>
			<span class="headline_lw">
				{!! trans('localization.info_finals')!!}
			</span>
			<p class="content_p bl">
				{!! trans('localization.info_finals_1')!!}
			</p>
			<br>
		</div>
        <br><br>
    </div>

    <div class="clear"></div>

    <div class="content_headline">
        {!! trans('localization.contact_headline')!!}
    </div>
    <div class="content"><!-- KONTAKT -->
        <br>
        <div class="boxes_wrap">
            <div>
                <a id="contact" style="display: inline-block; padding-top: 150px; margin-top:-150px">&nbsp;</a>
                <span class="email"><a href="mailto:shpinec@sh.cvut.cz">shpinec@sh.cvut.cz</a></span><br>
            </div>
            <br>
        </div>
        <div class="boxes_wrap" style="margin-top: 20px;">
            <div class="clear"></div>
            <div class="contact_box">
                <div class="contact_name">
                    MAROŠ KARAS
                </div>
                <div class="contact_content">
                    <div class="contact_pic">
                        <img src="<?php echo asset("profilovky/maros.jpg")?>" height="150"
                             alt="Maros Karas profil foto">
                    </div>
                    <div class="contact_text">
                        {!! trans('localization.contact_karas')!!}
                    </div>
                </div>
            </div>
            <div class="contact_box">
                <div class="contact_name">
                    LUBOŠ MAREK
                </div>
                <div class="contact_content">
                    <div class="contact_pic">
                        <img src="<?php echo asset("profilovky/lubos.jpg")?>" height="150"
                             alt="Lubos Marek profil foto">
                    </div>
                    <div class="contact_text">
                        {!! trans('localization.contact_marek')!!}
                    </div>
                </div>
            </div>
            <div class="contact_box">
                <div class="contact_name">
                    JAN PROCHÁZKA
                </div>
                <div class="contact_content">
                    <div class="contact_pic">
                        <img src="<?php echo asset("profilovky/honza.jpg")?>" height="150"
                             alt="Jan Prochazka profil foto">
                    </div>
                    <div class="contact_text">
                        {!! trans('localization.contact_prochazka')!!}
                    </div>
                </div>
            </div>

            <div class="contact_box">
                <div class="contact_name">
                    JAN ČADA
                </div>
                <div class="contact_content">
                    <div class="contact_pic">
                        <img src="<?php echo asset("profilovky/jan.jpg")?>" height="150" alt="Jan Cada profil foto">
                    </div>
                    <div class="contact_text">
                        {!! trans('localization.contact_cada')!!}
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div><!-- Konec-->
    <br><br>


    @include('partials.errors')
@endsection
