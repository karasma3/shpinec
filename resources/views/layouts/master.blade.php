<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../img/favicon.ico">

  <title>SH Pinec</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

@yield('head')
<!-- Custom styles for this template -->
<!--    {{--<link href="/css/blog.css" rel="stylesheet">--}} -->


<!--<link href="{{ mix('css/app.css') }}" rel="stylesheet">-->
  <link href="/css/pinec.css" rel="stylesheet">
  <link href="/css/bracket.css" rel="stylesheet">
  <link href="/css/global.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  {{--<link href="/css/sticky-footer-navbar.css" rel="stylesheet">--}}

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>

</head>

<body>

@include('partials.nav')

<!--CONTENT HERE-->
<div class="site-wrap">
  {{--MSG and FAILS--}}
  @if($flash = session('message'))
    <div class="alert alert-success" role="alert" style="margin-bottom: 0">
      {{ $flash }}
    </div>
  @endif
  @if($flash = session('fail'))
    <div class="alert alert-danger" role="alert" style="margin-bottom: 0">
      {{ $flash }}
    </div>
  @endif

<!--CONTENT HERE-->
  @yield('content')
</div>
@include('partials.footer')


<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script src="{{ mix('js/app.js') }}"></script>

@yield('scripts')

</body>
</html>
