<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language File
    |--------------------------------------------------------------------------
    |
    |
    */

    'nav_home' => 'DOMŮ',
    'nav_tournaments' => 'TURNAJE',
    'nav_stats' => 'STATISTIKY',
    'nav_login' => 'Přihlásit se',
    'nav_logout' => 'Odhlásit se',
    'nav_switch_language' => 'Switch language to English',
    'shpinec_headline' => 'STRAHOVSKÁ LIGA',
    'info_headline' => 'INFORMACE',
    'contact_headline' => 'KONTAKT',
    'gallery_headline' => 'FOTOGALERIE',
    'back' => 'Zpět',
    'back_to_semesters' => 'Zpět na všechny',
    'back_to_semester' => 'Zpět na semestr',
    'back_to_tournament' => 'Zpět na turnaj',
    'register' => 'Registrovat',
	'registration_closed' => 'Již nelze',
    'introduction_1' => 'Vítejte na stránkách Strahovské ligy ve stolním tenise.',
    'introduction_2' => 'Harmonogram a informace týkající se aktuálního ročníku naleznete na stránce <a href="/semesters">Turnaje</a>.<br>',
    'introduction_3' => 'V případě jakýchkoliv dotazů nás neváhejte kontaktovat přes <a href="#contact">e-mail</a>.',
    'info_system' => 'HERNÍ SYSTÉM',
    'info_system_1' => 'Od zimního semestru 2018/2019 se Strahovská liga ve stolním tenise hraje ve formě několika turnajů. Za umístění v turnaji se získávají body. Body se zapisují do žebříčku a nejlepší hráči podle tohoto žebříčku postupují do závěrečného turnaje. Posledním turnajem je velké finále na konci semestru, kde se rozhodne o výsledném pořadí účastníků Strahovské ligy ve stolním tenise. ',
    'info_rules' => 'PRAVIDLA TURNAJŮ',
    'info_rules_1' => '<ul><li>! neplatí pro závěrečný turnaj</li>
    <li>Počet účastníků není omezen</li>
    <li>Registrace na turnaj je nutná 15 minut předem</li>
    <li>Pro hru platí pravidla ITTF (<a style="color: white;" href="https://www.ittf.com/handbook/">https://www.ittf.com/handbook/</a>)</li>
    <li>Ceny pro vítěze</li>
    <li><span style="color: rgb(255, 150, 100)">Započítávají se 3 nejlepší výsledky z turnajů, tzn. lze jeden vynechat bez ztráty bodů<span></li></ul>',
    'info_points' => 'BODOVÁNÍ',
    'info_points_1' => '<ul>
    Body za umístnění v pavouku:
    <ul>
        <li>1. místo +10 bodů</li>
        <li>2. místo +9 bodů</li>
        <li>3. místo +8 bodů</li>
        <li>4. místo +7 bodů</li>
        <li>5.- 8. místo +6 bodů</li>
        <li>9. až poslední místo +5 bodů</li>
    </ul>
    Body za umístnění v pavouku útěchy:
    <br>
    <ul>
        <li>1. místo +4 body</li>
        <li>2. místo +3 body</li>
        <li>3. - 4. místo +2 body</li>
        <li>5. až poslední místo +1 bod</li>
    </ul>
                </ul>',
    'info_finals' => 'ZÁVEREČNÝ TURNAJ (VEČÍREK)',
    'info_finals_1' => 'Na konci semestru se 8 nejlepších hráčů utká o titul velmistra Strahovské ligy ve stolním tenise a vítězové dostanou poháry a věcné ceny.<br>
    Na tuto velkolepou podívanou jsou zvaní všichni účastníci a příznivci stolního tenisu.<br> 
    V průběhu závěrečného večírku bude zajištěné občerstvení a pitný režim.',
    'contact_info' => 'V případě, že máte nějaké dotazy ohledně ligy, kontaktujte nás.',
    'contact_karas' => '<br><br>
                        m.karas@sh.cvut.cz<br>
                        Hlavní organizátor ligy',
    'contact_marek' => '<br><br>
                        l.marek@sh.cvut.cz<br>
                        Organizátor<br>
                        Autor webu',
    'contact_prochazka' => '<br><br>
                        j.prochazka@sh.cvut.cz<br>
                        Organizátor',
    'contact_cada' => '<br><br>
                        jan.cada@sh.cvut.cz<br>
                        Organizátor',
    'login' => 'Přihlášení',
    'registration' => 'Registrace',
    'locality' => 'Místo',
    'date' => 'Datum',
    'save' => 'Uložit',
    'save_information' => 'Uložit údaje',
    'change_password' => 'Změň heslo',
    'matches_to_be_played' => 'Neodehrané zápasy',
    'match_history' => 'Histórie zápasů',
    'information' => 'Informace',
    'name' => 'Jméno',
    'surname' => 'Přijmení',
    'password' => 'Heslo',
    'current_password' => 'Staré heslo',
    'new_password' => 'Nové heslo',
    'password_confirmation' => 'Potvrzení nového hesla',
    'email' => 'E-mail',
    'current' => 'Aktuální',
    'current_not_found' => 'Aktuálně neprobíhá žádný semestr',
    'previous' => 'Předchozí',
    'semester' => 'Semestr',
    'tournament' => 'Turnaj',
    'tournaments' => 'Turnaje',
    'semester_standings' => 'Celkové výsledky v semestru',
    'tournament_standings' => 'Celkové výsledky turnaje',
    'final_tournament_standings' => 'Výsledky posledního turnaje (Celkově ligy)',
    'table' => 'Tabulka',
    'bracket' => 'Pavouk',
    'loser_bracket' => 'Pavouk útěchy',
    'team' => 'Tým',
    'teams' => 'Týmy',
    'group' => 'Skupina',
    'groups' => 'Skupiny',
    'match' => 'Zápas',
    'matches' => 'Zápasy',
    'score' => 'Skóre',
    'points' => 'Body',
    'player' => 'Hráč',
    'players' => 'Hráči',
    'opponent' => 'Soupeř',
    'wins' => 'Výhry',
    'elo' => 'W/L ratio',
    'registered_players' => 'Počet přihlášených hráčů',
    'number_matches_played' => 'Počet odehraných zápasů',
    'admin_section' => 'Administrace',
    'create_semester' => 'Vytvořit semestr',
    'create_tournament' => 'Vytvořit turnaj',
    'send_email_to_participants' => 'Pošli hromadný email účastníkům',
    'send_email_elimination_stage' => 'Pošli hromadný email účastníkům vyřazovací části',
    'registered_teams' => 'Přihlášené týmy',
    'quick_navigation' => 'Rychlá navigace',
    'select_option' => '-- vyber možnost --',
    'semester_season' => 'Sezóna',
    'semester_year' => 'Akademický rok',
    'activate' => 'Aktivuj',
    'deactivate' => 'Deaktivuj',
    'semester_delete' => 'Vymaž',
    'generate_groups' => 'Generuj skupiny',
    'calculate_score' => 'Přepočítej výsledky skupin',
    'create_bracket' => 'Vytvor pavúka',
    'next_round' => 'Další kolo',
    'close_tournament' => 'Ukonči turnaj',
    'match_edit' => 'Zapiš výsledek',
    'match_result' => 'Výslední skóre',
    'order' => 'Pořadí',
    'order_short' => '#',
    'matches_all' => 'Z',
    'matches_wins' => 'V',
    'matches_loses' => 'P',
    'score_in_group' => 'Skóre ve skupině',
    'points_in_group' => 'Výhry ve skupině',
    'score_total' => 'Skóre celkem',
    'points_total' => 'Body celkem',
    'round' => 'Kolo',
    'inactive' => 'Neaktivní',
    'add_team' => 'Přidej tým',
    'create_groups' => 'Vytvoř skupiny',
    'create_matches' => 'Vytvoř zápasy',
    'state' => 'Stav',
    'set_next_state' => 'Změň stav na další',
    'doubles_create' => 'Vytvoř tým pro čtyřhru',
    'doubles_email_first' => 'E-mail prvního hráče',
    'doubles_email_second' => 'E-mail druhýho hráče',
    'final_tournament' => 'Závěrečný turnaj',
    'tournament_lowercase' => 'turnaj',
    'remove_team' => 'Odhlaš',
//    tournament phases
    'tournament_phase_registration' => 'Registrace',
    'tournament_phase_group_stage' => 'Skupinová část',
    'tournament_phase_elimination_stage' => 'Vyřaďovací část',
    'tournament_phase_closed' => 'Ukončen',
//    msgs
    'msg_login' => 'Byl si úspěšně přihlášen',
    'msg_logout' => 'Byl si úspěšně odhlášen',
    'msg_submit_score' => 'Skóre uloženo',
    'msg_register' => 'Byl si úspěšně zaregistrovaný',
    'msg_semester_created' => 'Semestr byl vytvořen',
    'msg_team_created' => 'Tým byl vytvořen',
    'msg_tournament_info_saved' => 'Informace o turnaji byly uloženy',
    'msg_tournament_groups_create' => 'Skupiny byly vytvořeny',
    'msg_tournament_bracket_create' => 'Pavúk byl vytvořen',
    'msg_password_change' => 'Tvé heslo bylo úspěšně změněno',
    'msg_player_info_change' => 'Tvoje informace byly úspěšně změneny',
    'msg_tournament_close' => 'Turnaj byl ukončen',
    'msg_tournament_next_round' => 'Další kolo bylo vytvořeno',
    'msg_tournament_register' => 'Tým úspěšně přihlášen na turnaj',
    'msg_tournament_remove_registration' => 'Tým úspěšně odhlášen z turnaje',
    'msg_tournament_matches_create' => 'Zápasy byly vytvořeny',
    'msg_group_register' => 'Tým úspěšně přidán do skupiny',
//    fails
    'fail_match_draw' => 'Zápas nemůže skončit remízou',
    'fail_semester_exists' => 'Semestr již existuje',
    'fail_team_exists' => 'Tým již existuje',
    'fail_final_tournament_exists' => 'Závěrečný turnaj byl již vytvořen',
    'fail_doubles_player_not_found' => 'Hráč s emailem :email nebyl nalezen',
    'fail_password_wrong' => 'Nesprávné heslo',
    'fail_tournament_not_final_round' => 'Nebyl odehrán finálový zápas',
    'fail_tournament_all_matches' => 'Všechny zápasy musí být odehrány',
    'fail_tournament_lower_limit' => 'Turnaje se musí zúčastnit alespoň :count týmov',
    'fail_tournament_bracket_lower_limit' => 'Ze skupinové části musí postoupit alespoň :count týmy',
    'fail_tournament_upper_limit' => 'Turnaj je zcela naplněn :count týmy',
    'fail_group_upper_limit' => 'Skupina je zcela naplněna',
    'fail_tournament_final_round_exists' => 'Turnaj již je vo finálovém kole',
    'fail_tournament_register' => 'Tým je již přihlásen do turnaje',
    'fail_tournament_remove_registration' => 'Tým není přihlášený na turnaj',
    'fail_group_register' => 'Tým je již přihlásen do skupiny',
    'fail_no_permission' => 'Nemáš na to právo',
];