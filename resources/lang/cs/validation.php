<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Atribut :attribute musí být schopen přijmout',
    'active_url'           => 'URL :attribute není platné',
    'after'                => 'Datum :attribute musí být větší než :date',
    'after_or_equal'       => 'Datum :attribute musí být větší nebo rovný :date',
    'alpha'                => 'Atribut :attribute může obsahovat jenom písmena',
    'alpha_dash'           => 'Atribut :attribute může obsahovat jenom písmena, čísla a pomlčky',
    'alpha_num'            => 'Atribut :attribute může obsahovat jenom písmena a čísla',
    'array'                => 'Attribut :attribute musí být typu array',
    'before'               => 'Datum :attribute musí být menší než :date',
    'before_or_equal'      => 'Datum :attribute musí být menší nebo rovný :date',
    'between'              => [
        'numeric' => 'Číslo :attribute musí být mezi :min a :max',
        'file'    => 'Soubor :attribute musí mít mezi :min a :max KB',
        'string'  => 'Řetězec :attribute musí mít mezi :min a :max znaků',
        'array'   => 'Array :attribute musí obsahovat mezi :min a :max položek',
    ],
    'boolean'              => 'Atribut :attribute musí být typu boolean',
    'confirmed'            => 'Potvrzení :attribute se nezhoduje',
    'date'                 => 'Datum :attribute není platný',
    'date_format'          => 'Datum :attribute nemá správny formát :format',
    'different'            => 'Atribut :attribute a atribut :other musí být odlišné',
    'digits'               => 'Číslo :attribute musí mít :digits číslovek',
    'digits_between'       => 'Číslo :attribute musí mít mezi :min a :max číslovek',
    'dimensions'           => 'Obrázek :attribute má špatný rozměr',
    'distinct'             => 'Pole :attribute s danou hodnotou již existuje',
    'email'                => 'Email :attribute není platný',
    'exists'               => 'Vybraný atribut :attribute není platný',
    'file'                 => 'Atribut :attribute musí být typu soubor',
    'filled'               => 'Pole :attribute musí být vyplněno',
    'image'                => 'Atribut :attribute musí být typu obrázek',
    'in'                   => 'Vybraný atribut :attribute není platný',
    'in_array'             => 'Atribut :attribute neexistuje v :other.',
    'integer'              => 'Atribut :attribute musí být typu integer',
    'ip'                   => 'IP adresa :attribute musí být platný',
    'ipv4'                 => 'IPv4 adresa :attribute musí být platný',
    'ipv6'                 => 'IPv6 adresa :attribute musí být platný',
    'json'                 => 'JSON :attribute musí být platný',
    'max'                  => [
        'numeric' => 'Číslo :attribute nesmí být větší než :max',
        'file'    => 'Soubor :attribute nesmí mít víc než :max KB',
        'string'  => 'Řetězec :attribute nesmí mít víc než :max znaků',
        'array'   => 'Array :attribute nesmí mít víc než :max položek',
    ],
    'mimes'                => 'Soubour :attribute musí být typu: :values',
    'mimetypes'            => 'Soubour :attribute musí být typu: :values',
    'min'                  => [
        'numeric' => 'Číslo :attribute musí být alespoň :min',
        'file'    => 'Soubor :attribute musí mít alespoň :min KB',
        'string'  => 'Řetězec :attribute musí být alespoň :min znaků',
        'array'   => 'Array :attribute musí obsahovat alespoň :min položky',
    ],
    'not_in'               => 'Vybraný :attribute je neplatný',
    'numeric'              => 'Pole :attribute musí být číslo',
    'present'              => 'Pole :attribute musí být přítomno',
    'regex'                => 'Formát :attribute je neplatný',
    'required'             => 'Pole pro :attribute je povinné',
    'required_if'          => 'Pole pro :attribute je povinné když :other je :value',
    'required_unless'      => 'Pole pro :attribute je povinné pokud :other není v :values',
    'required_with'        => 'Pole pro :attribute je povinné když :values jsou přítomny',
    'required_with_all'    => 'Pole pro :attribute je povinné když :values jsou přítomny',
    'required_without'     => 'Pole pro :attribute je povinné když :values nejsou přítomny',
    'required_without_all' => 'Pole pro :attribute je povinné když žádna z :values není přítomna',
    'same'                 => 'Pole pro :attribute a :other se musí rovnat',
    'size'                 => [
        'numeric' => 'Číslo :attribute musí být :size',
        'file'    => 'Soubour :attribute musí mít :size KB',
        'string'  => 'Řetězec :attribute musí mít :size znaků',
        'array'   => 'Array :attribute musí obsahovat :size položky',
    ],
    'string'               => 'Atribut :attribute musí být řetězec',
    'timezone'             => 'Atribut :attribute musí mít platnou časovou zónu',
    'unique'               => 'Atribut :attribute již existuje',
    'uploaded'             => 'Nahrávání atributu :attribute se nezdařilo',
    'url'                  => 'Formát atributu :attribute není platný',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'jméno',
        'surname' => 'přijmení',
        'email' => 'e-mail',
        'email_first' => 'první e-mail',
        'email_second' => 'druhý e-mail',
        'password' => 'heslo',
        'current-password' => 'aktuální heslo',
        'semester_season' => 'sezóna',
        'semester_year' => 'akademický rok',
        'team_id' => 'tým',
        'team_name' => 'tým',
        'score_first' => 'první skóre',
        'score_second' => 'druhé skóre',
    ],

];