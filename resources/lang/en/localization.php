<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language File
    |--------------------------------------------------------------------------
    |
    |
    */

    'nav_home' => 'HOME',
    'nav_tournaments' => 'TOURNAMENTS',
    'nav_stats' => 'STATS',
    'nav_login' => 'Login',
    'nav_logout' => 'Logout',
    'nav_switch_language' => 'Přepnout jazyk na češtinu',
    'shpinec_headline' => 'STRAHOV LEAGUE',
    'info_headline' => 'INFORMATION',
    'contact_headline' => 'CONTACT',
    'gallery_headline' => 'GALLERY',
    'back' => 'Back',
    'back_to_semesters' => 'Back to all semesters',
    'back_to_semester' => 'Back to semester',
    'back_to_tournament' => 'Back to tournament',
    'register' => 'Register',
	'registration_closed' => 'Unavailable',
    'introduction_1' => 'Welcome to official website of Strahov Table Tennis League.',
    'introduction_2' => 'Details about current run are on the page <a href="/semesters">Tournaments</a>.<br>',
    'introduction_3' => 'In case you have any questions, don\'t hesitate to contact us via <a href="#contact">e-mail</a>.',
    'info_system' => 'LEAGUE SYSTEM',
    'info_system_1' => 'Starting winter semester 2018/2019, Strahov Table Tennis League is played in smaller tournaments resulting in a final tourmanent.
                    Each player gets appropriate amount of points according to his/her position in a tournament.<br>
                    Points are written down in the ranking ladder and the best players qualify for the final tournament.<br>
                    The final tournament will be the Grand finale of the season and will take place in the end of the semester.',
    'info_rules' => 'TOURNAMENT RULES',
    'info_rules_1' => '<ul>
                    <li>! excluding final tournament</li>
                    <li>Number of participants is not limited</li>
                    <li>Registrations are open until 15 minutes before the tournament starts</li>
                    <li>ITTF rules (<a style="color: white;" href="https://www.ittf.com/handbook/">https://www.ittf.com/handbook/</a>)</li>
                    <li>Awards for winners</li>
                    <li><span style="color: rgb(255, 150, 100)">
                        Only 3 best tournament results count - you can miss one and still get the maximum<span>
                    </li>
                </ul>',
    'info_points' => 'POINT SYSTEM',
    'info_points_1' => '<ul>
    Points for placement in bracket:
    <br>
    <ul>
        <li>1st place +10 pts</li>
        <li>2nd place +9 pts</li>
        <li>3rd place +8 pts</li>
        <li>4th place +7 pts</li>
        <li>5th - 8th place +6 pts</li>
        <li>9th - last place +5 pts</li>
    </ul>
    <br>
    Points for placement in loser bracket:
    <br>
    <ul>
        <li>1st place +4 pts</li>
        <li>2nd place +3 pts</li>
        <li>3rd - 4th place +2 pts</li>
        <li>5th - last place +1 pt</li>
    </ul>
                </ul>',
    'info_finals' => 'GRAND FINALE (PARTY)',
    'info_finals_1' => 'In the end of the semester, 8 best players will decide final standings of the season.
                    However, only one can take the imaginary throne of Strahov league.<br>
                    Winners will get their trophies and other tangible rewards.<br>
                    All tournament participants and fans of table tennis are invited to wittness this event.<br>
                    You don\'t have to worry about refreshment or beer, we will provide more than you need',
    'contact_info' => 'In case you have any questions about league, contact us.',
    'contact_karas' => '<br><br>
                        m.karas@sh.cvut.cz<br>
                        League director',
    'contact_marek' => '<br><br>
                        l.marek@sh.cvut.cz<br>
                        Organizer<br>
                        Web author',
    'contact_prochazka' => '<br><br>
                        j.prochazka@sh.cvut.cz<br>
                        Organizer',
    'contact_cada' => '<br><br>
                        jan.cada@sh.cvut.cz<br>
                        Organizer',
    'login' => 'Login',
    'registration' => 'Registration',
    'locality' => 'Locality',
    'date' => 'Date',
    'save' => 'Save',
    'save_information' => 'Save information',
    'change_password' => 'Change password',
    'information' => 'Information',
    'name' => 'Name',
    'surname' => 'Surname',
    'matches_to_be_played' => 'Matches to be played',
    'match_history' => 'Matches history',
    'password' => 'Password',
    'current_password' => 'Old password',
    'new_password' => 'New password',
    'password_confirmation' => 'Password confirmation',
    'email' => 'Email',
    'current' => 'Current',
    'current_not_found' => 'There is currently no active semester',
    'previous' => 'Previous',
    'semester' => 'Semester',
    'tournament' => 'Tournament',
    'tournaments' => 'Tournaments',
    'semester_standings' => 'Semester standings',
    'tournament_standings' => 'Tournament standings',
    'final_tournament_standings' => 'Final tournament standings (League standings)',
    'table' => 'Table',
    'bracket' => 'Bracket',
    'loser_bracket' => 'Loser bracket',
    'team' => 'Team',
    'teams' => 'Teams',
    'group' => 'Group',
    'groups' => 'Groups',
    'match' => 'Match',
    'matches' => 'Matches',
    'score' => 'Score',
    'points' => 'Points',
    'player' => 'Player',
    'players' => 'Players',
    'opponent' => 'Opponent',
    'wins' => 'Wins',
    'elo' => 'W/L ratio',
    'registered_players' => 'Number of registered players',
    'number_matches_played' => 'Number of played matches',
    'admin_section' => 'Admin section',
    'create_semester' => 'Create semester',
    'create_tournament' => 'Create tournament',
    'send_email_to_participants' => 'Send email to all participants',
    'send_email_elimination_stage' => 'Send email to participants in elimination stage',
    'registered_teams' => 'Registered teams',
    'quick_navigation' => 'Quick navigation',
    'select_option' => '-- select option --',
    'semester_season' => 'Season',
    'semester_year' => 'Academic year',
    'activate' => 'Activate',
    'deactivate' => 'Deactivate',
    'semester_delete' => 'Delete',
    'generate_groups' => 'Generate groups',
    'calculate_score' => 'Recalculate group results',
    'create_bracket' => 'Create bracket',
    'next_round' => 'Next round',
    'close_tournament' => 'Close tournament',
    'match_edit' => 'Submit result',
    'match_result' => 'Match result',
    'order' => 'Order',
    'order_short' => '#',
    'matches_all' => 'M',
    'matches_wins' => 'W',
    'matches_loses' => 'L',
    'score_in_group' => 'Score in group',
    'points_in_group' => 'Wins in group',
    'score_total' => 'Score total',
    'points_total' => 'Points total',
    'round' => 'Kolo',
    'inactive' => 'Inactive',
    'add_team' => 'Add team',
    'create_groups' => 'Create groups',
    'create_matches' => 'Create matches',
    'state' => 'State',
    'set_next_state' => 'Set next state',
    'doubles_create' => 'Create doubles team',
    'doubles_email_first' => 'E-mail first player',
    'doubles_email_second' => 'E-mail second player',
    'final_tournament' => 'Final tournament',
    'tournament_lowercase' => 'tournament',
    'remove_team' => 'Remove team',
    //    tournament phases
    'tournament_phase_registration' => 'Registration',
    'tournament_phase_group_stage' => 'Group stage',
    'tournament_phase_elimination_stage' => 'Elimination stage',
    'tournament_phase_closed' => 'Closed',
//    msgs
    'msg_login' => 'You have been successfully logged in',
    'msg_logout' => 'You have been successfully logged out',
    'msg_submit_score' => 'Score saved',
    'msg_register' => 'You have been successfully registered',
    'msg_semester_created' => 'Semester was successfully created',
    'msg_team_created' => 'Team was successfully created',
    'msg_tournament_info_saved' => 'Information about tournament were successfully saved',
    'msg_tournament_groups_create' => 'Groups were successfully created',
    'msg_tournament_bracket_create' => 'Bracket was successfully created',
    'msg_password_change' => 'Your password was successfully changed',
    'msg_player_info_change' => 'Your information has been successfully changed',
    'msg_tournament_close' => 'Tournament was successfully closed',
    'msg_tournament_next_round' => 'Next round was successfully created',
    'msg_tournament_register' => 'Successfully registered into the tournament',
    'msg_tournament_remove_registration' => 'Team was successfully removed from the tournament',
    'msg_tournament_matches_create' => 'Matches were successfully created',
    'msg_group_register' => 'Team successfully added into the group',
//    fails
    'fail_match_draw' => 'The match cannot end in a draw',
    'fail_semester_exists' => 'Semester already exists',
    'fail_team_exists' => 'Team already exists',
    'fail_final_tournament_exists' => 'Final tournament already exists',
    'fail_doubles_player_not_found' => 'Player with email :email was not found',
    'fail_password_wrong' => 'Wrong password',
    'fail_tournament_not_final_round' => 'Final match must be played',
    'fail_tournament_all_matches' => 'All matches must be played',
    'fail_tournament_lower_limit' => 'Tournament must have at least :count teams',
    'fail_tournament_bracket_lower_limit' => 'There must be at least :count teams to advance from group stage',
    'fail_tournament_upper_limit' => 'Tournament is full with :count teams',
    'fail_group_upper_limit' => 'Group is full',
    'fail_tournament_final_round_exists' => 'Tournament is already in final round',
    'fail_tournament_register' => 'This team is already signed in',
    'fail_tournament_remove_registration' => 'This team is not signed in',
    'fail_group_register' => 'Team is already signed in',
    'fail_no_permission' => 'You have no permission to do that',
];