<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{locale}', 'HomeController@lang');

Route::get('/', function () {
    return view('welcome');
}) -> name('home');

Route::get('/tournaments/{tournament}/export', 'TournamentController@export');
Route::get('/semesters/{semester}/export', 'SemesterController@export');

Route::group(['middleware' => ['auth', 'is_admin']], function () {
    Route::post('/semesters', 'SemesterController@store');
    Route::post('/semesters/{semester}/setActive', 'SemesterController@setActive');
    Route::post('/semesters/{semester}/addTournament', 'SemesterController@addTournament');
    Route::delete('/semesters/{semester}/delete', 'SemesterController@delete');
});
Route::get('/semesters', 'SemesterController@index');
Route::get('/semesters/{semester}', 'SemesterController@show');

Route::group(['middleware' => ['auth', 'is_admin']], function () {
    Route::post('/tournaments/{tournament}/save_info', 'TournamentController@saveInfo');
    Route::post('/tournaments/{tournament}/generate_groups', 'TournamentController@generateGroups');
    Route::post('/tournaments/{tournament}/create_bracket', 'TournamentController@createBracket');
    Route::patch('/tournaments/{tournament}/next_round', 'TournamentController@nextRound');
    Route::patch('/tournaments/{tournament}/nextLoserBracket', 'TournamentController@nextLoserBracket');
    Route::patch('/tournaments/{tournament}/calculate_score', 'TournamentController@calculateScore');
    Route::post('/tournaments/{tournament}/close', 'TournamentController@close');
//    manualne vytvaranie skupiny a zapasov, nastavovanie dalsej fazy
//    Route::post('/tournaments/{tournament}/create_groups', 'TournamentController@createGroups');
//    Route::post('/tournaments/{tournament}/create_matches', 'TournamentController@createMatches');
//    Route::post('/tournaments/{tournament}/set_next_state', 'TournamentController@setNextState');
});
Route::group(['middleware' => ['auth']], function () {
    Route::post('/tournaments/{tournament}/join', 'TournamentController@addTeam');
});
Route::group(['middleware' => ['auth', 'tournament_player_remove']], function () {
    Route::post('/tournaments/{tournament}/removeTeam', 'TournamentController@removeTeam');
});
Route::get('/tournaments/{tournament}', 'TournamentController@show');

Route::group(['middleware' => ['auth', 'is_admin']], function () {
//    manualne pridavanie timov do skupiny
//    Route::post('/groups/{group}/join', 'GroupController@addTeam');
});

Route::get('/groups/{group}', 'GroupController@show');
Route::get('/loserBracket/{tournament}', 'TournamentController@showLoserBracket');

Route::group(['middleware' => ['auth', 'match_user_edit']], function () {
    Route::patch('/matches/{match}', 'MatchController@submitScore');
});
Route::get('/matches/{match}', 'MatchController@show');

// TODO zatial to nechame na adminovi - pripravene na stovrhry
Route::group(['middleware' => ['auth', 'is_admin']], function () {
    Route::get('/teams/create', 'TeamController@create');
    Route::post('/teams', 'TeamController@store');
    Route::get('/teams/{team}', 'TeamController@show');
    Route::post('/teams/{team}/inactivate', 'TeamController@inactivateTeam');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/players/{player}', 'PlayerController@show');
    Route::patch('/players/{player}', 'PlayerController@editPlayer');
    Route::patch('/players/{player}/password', 'PlayerController@changePassword');
});

// Authentication Routes...
Route::group(['middleware' => ['guest']], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});

// Registration Routes...
Route::group(['middleware' => ['guest']], function () {
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
});

Route::group(['middleware' => ['auth', 'is_organizer']], function () {
    Route::post('register/join/{tournament}', 'Auth\RegisterController@registerAndJoin');
});

// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');
